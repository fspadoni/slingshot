//
//  CCPhysicsShaders.h
//  cocos2d_libs
//
//  Created by Federico Spadoni on 07/03/16.
//
//

#ifndef __cocos2d_libs__CCPhysicsShaders__
#define __cocos2d_libs__CCPhysicsShaders__

#include "platform/CCGL.h"
#include "platform/CCPlatformMacros.h"

/**
 * @addtogroup renderer
 * @{
 */

NS_CC_BEGIN

extern CC_DLL const GLchar * ccShader_2D_def_vert;
extern CC_DLL const GLchar * ccShader_2D_def_frag;
extern CC_DLL const GLchar * ccShader_2D_LINE_vert;
extern CC_DLL const GLchar * ccShader_2D_LINE_frag;
extern CC_DLL const GLchar * ccShader_2D_LINE_POW_vert;
extern CC_DLL const GLchar * ccShader_2D_LINE_POW_frag;
extern CC_DLL const GLchar * ccShader_2D_Bullet_vert;
extern CC_DLL const GLchar * ccShader_2D_Bullet_frag;
extern CC_DLL const GLchar * ccShader_2D_BloomExtract_vert;
extern CC_DLL const GLchar * ccShader_2D_BloomExtract_frag;
extern CC_DLL const GLchar * ccShader_2D_BloomBlur_vert;
extern CC_DLL const GLchar * ccShader_2D_BloomBlur_frag;
NS_CC_END


#endif /* defined(__cocos2d_libs__CCPhysicsShaders__) */
