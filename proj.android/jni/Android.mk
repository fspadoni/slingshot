LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

$(call import-add-path,$(LOCAL_PATH)/../../cocos2d)
$(call import-add-path,$(LOCAL_PATH)/../../cocos2d/external)
$(call import-add-path,$(LOCAL_PATH)/../../cocos2d/cocos)

LOCAL_MODULE := cocos2dcpp_shared

LOCAL_MODULE_FILENAME := libcocos2dcpp

LOCAL_SRC_FILES := hellocpp/main.cpp \
		       ../../Classes/AppDelegate.cpp \
                   ../../Classes/HelloWorldScene.cpp \
                   ../../Classes/Main.cpp \
                   ../../Classes/LevelLoader.cpp \
                   ../../Classes/GameScene.cpp \
                   ../../Classes/GameLayer.cpp \
                   ../../Classes/GameConfig.cpp \
                   ../../Classes/SlingShot.cpp \
                   ../../Classes/SlingShotStatus.cpp \
                   ../../Classes/Bullet.cpp \
                   ../../Classes/BulletStatus.cpp \
                   ../../Classes/Asteroid.cpp \
                   ../../Classes/GameScene1.cpp \
                   ../../Classes/GameLayer1.cpp \

LOCAL_C_INCLUDES := $(LOCAL_PATH)/../../Classes

# _COCOS_HEADER_ANDROID_BEGIN
# _COCOS_HEADER_ANDROID_END


LOCAL_STATIC_LIBRARIES := cocos2dx_static
LOCAL_STATIC_LIBRARIES := cocos2d_lua_static

# _COCOS_LIB_ANDROID_BEGIN
# _COCOS_LIB_ANDROID_END

include $(BUILD_SHARED_LIBRARY)

$(call import-module,scripting/lua-bindings/proj.android)
$(call import-module,.)

# _COCOS_LIB_IMPORT_ANDROID_BEGIN
# _COCOS_LIB_IMPORT_ANDROID_END

#export LC_ALL=en_US.UTF-8
#export LANG=en_US.UTF-8
#source ~/.bash_profile