//
//  LevelList.cpp
//  Slingshot
//
//  Created by Federico Spadoni on 24/10/16.
//
//

#include "LevelList.h"

#include "Main.h"
#include "LevelScene.h"
#include "GameScene1.h"


USING_NS_CC;


 LevelCustomTableView* LevelCustomTableView::create(cocos2d::extension::TableViewDataSource* dataSource, Size size)
{
    auto table = new (std::nothrow) LevelCustomTableView();
    table->initWithViewSize(size, nullptr);
    table->autorelease();
    table->setDataSource(dataSource);
    table->_updateCellPositions();
    table->_updateContentSize();
    
    return table;
}

LevelCustomTableView::LevelCustomTableView()
{
    auto mouseListener = EventListenerMouse::create();
    mouseListener->onMouseScroll = CC_CALLBACK_1(LevelCustomTableView::onMouseScroll, this);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(mouseListener, this);
}

void LevelCustomTableView::onTouchEnded(Touch *touch, Event *event)
{
    if (!this->isVisible())
    {
        return;
    }
    
    if (_touchedCell)
    {
        auto label = (Label*)_touchedCell->getChildByTag(_TABEL_LABEL_TAG);
        
        Rect bbox = label->getBoundingBox();
        bbox.origin = _touchedCell->convertToWorldSpace(bbox.origin);
        
        if (bbox.containsPoint(touch->getLocation()) && _tableViewDelegate != nullptr)
        {
            _tableViewDelegate->tableCellUnhighlight(this, _touchedCell);
            _tableViewDelegate->tableCellTouched(this, _touchedCell);
        }
        
        _touchedCell = nullptr;
    }
    
    ScrollView::onTouchEnded(touch, event);
}

void LevelCustomTableView::onMouseScroll(Event *event)
{
    auto mouseEvent = static_cast<EventMouse*>(event);
    float moveY = mouseEvent->getScrollY() * 20;
    
    auto minOffset = this->minContainerOffset();
    auto maxOffset = this->maxContainerOffset();
    
    auto offset = this->getContentOffset();
    offset.y += moveY;
    
    if (offset.y < minOffset.y)
    {
        offset.y = minOffset.y;
    }
    else if (offset.y > maxOffset.y)
    {
        offset.y = maxOffset.y;
    }
    this->setContentOffset(offset);
}



LevelList::LevelList()
{
    addLevel( "Level ", [](LevelLoader* loader){return GameScene1::create(loader); } );
}

void LevelList::addLevel(const std::string& levelName, std::function<cocos2d::Scene*(LevelLoader*)> callback)
{
    if (!levelName.empty())
    {
        std::string fullLevelName = levelName + std::to_string(_levelNames.size() + 1);
        _levelNames.emplace_back( fullLevelName );
        _levelCallbacks.emplace_back(callback);
    }
}


void LevelList::display()
{
    _cellTouchEnabled = true;
    auto director = Director::getInstance();
    auto scene = Scene::create();
    
    Size visibleSize = director->getVisibleSize();
    Vec2 origin = director->getVisibleOrigin();
    
    auto tableView = LevelCustomTableView::create(this, Size(400, visibleSize.height));
    tableView->setPosition(origin.x + (visibleSize.width - 400) / 2, origin.y);
    tableView->setDirection(cocos2d::extension::ScrollView::Direction::VERTICAL);
    tableView->setVerticalFillOrder(cocos2d::extension::TableView::VerticalFillOrder::TOP_DOWN);
    tableView->setDelegate(this);
    scene->addChild(tableView);
    tableView->reloadData();
    
    if (_shouldRestoreTableOffset)
    {
        tableView->setContentOffset(_tableOffset);
    }
    
    
    TTFConfig ttfConfig("fonts/arial.ttf", 20);
    auto label = Label::createWithTTF(ttfConfig, "Back");
    auto closeItem = MenuItemLabel::create(label, CC_CALLBACK_1(LevelList::onBacksToMainMenu, this) );
    closeItem->setPosition( origin+(visibleSize/2.0) );
    
    
    auto menu = Menu::create(closeItem, nullptr);
    menu->setPosition(Vec2::ZERO);
    scene->addChild(menu, 1);
    
    
    director->replaceScene(scene);
}


void LevelList::onBacksToMainMenu(Ref* sender)
{
    auto scene = Main::createScene();
    
    if (scene)
    {
        Director::getInstance()->replaceScene(scene);
    }
}

void LevelList::tableCellTouched(cocos2d::extension::TableView* table, cocos2d::extension::TableViewCell* cell)
{
    if (_cellTouchEnabled)
    {
        auto index = cell->getIdx();
        if (_levelCallbacks[index])
        {
            auto scene = _levelCallbacks[index](nullptr);
//            if (level->getChildTestCount() > 0)

                _tableOffset = table->getContentOffset();
                _shouldRestoreTableOffset = true;
                _cellTouchEnabled = false;
//                Scene* scene = level->create();
//                level->setTestParent(this);
//                level->runThisTest();

            if (scene)
            {
                Director::getInstance()->replaceScene(scene);
            }
            
        }
    }
}

cocos2d::extension::TableViewCell* LevelList::tableCellAtIndex(cocos2d::extension::TableView *table, ssize_t idx)
{
    auto cell = table->dequeueCell();
    if (!cell)
    {
        cell = cocos2d::extension::TableViewCell::create();
        auto label = Label::createWithTTF(_levelNames[idx], "fonts/arial.ttf", 25.0f);
        label->setTag(LevelCustomTableView::_TABEL_LABEL_TAG);
        label->setPosition(200, 15);
        cell->addChild(label);
    }
    else
    {
        auto label = (Label*)cell->getChildByTag(LevelCustomTableView::_TABEL_LABEL_TAG);
        label->setString(_levelNames[idx]);
    }
    
    return cell;
}

Size LevelList::tableCellSizeForIndex(cocos2d::extension::TableView *table, ssize_t idx)
{
    return Size(400, 40);
}

ssize_t LevelList::numberOfCellsInTableView(cocos2d::extension::TableView *table)
{
    return _levelNames.size();
}

