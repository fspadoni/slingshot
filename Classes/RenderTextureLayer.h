//
//  RenderTextureLayer.h
//  Slingshot
//
//  Created by Federico Spadoni on 11/03/16.
//
//

#ifndef __Slingshot__RenderTextureLayer__
#define __Slingshot__RenderTextureLayer__

#include "cocos2d.h"


class RenderTextureLayer : public cocos2d::Layer
{
  
public:
	static RenderTextureLayer* create(const std::string& vertexShaderFile, const std::string& fragmentShaderFile);
    
    static RenderTextureLayer* createWithByteArrays(const GLchar* vShaderByteArray, const GLchar* fShaderByteArray);

    
    virtual bool init(const std::string& vertexShaderFile, const std::string& fragmentShaderFile);
    
    virtual bool init(const GLchar* vShaderByteArray, const GLchar* fShaderByteArray);
    
//    void draw(cocos2d::Renderer* renderer, const cocos2d::Mat4 &transform, uint32_t flags) override;
    void visit(cocos2d::Renderer* renderer, const cocos2d::Mat4 &parentTransform, uint32_t parentFlags) override;
    
//    void draw(RenderTextureLayer* layer);
    
    cocos2d::RenderTexture* getRenderTexture() const { return _renderTexture; }
  
    cocos2d::Texture2D* getOutputTexture()  { return _renderTexture->getSprite()->getTexture(); }
    
    cocos2d::Size getTextureSize() const { return _renderTexture->getSprite()->getTexture()->getContentSize(); };
 
    
protected:
	RenderTextureLayer();
	virtual ~RenderTextureLayer();

    bool init();
    
    
private:
    
    
    
    cocos2d::RenderTexture* _renderTexture;
//    cocos2d::Sprite* _sprite;
    

};

#endif /* defined(__Slingshot__RenderTextureLayer__) */
