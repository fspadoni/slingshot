//
//  LevelScene.cpp
//  Slingshot
//
//  Created by Federico Spadoni on 23/10/16.
//
//

#include "LevelScene.h"

#include "GameScene1.h"


USING_NS_CC;


Scene* LevelLayer::createScene()
{
    
    auto scene = Scene::create();
    
    //    // 'layer' is an autorelease object
    auto layer = LevelLayer::create();
    
    
    //    // add layer as a child to scene
    scene->addChild(layer);
    
    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool LevelLayer::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    /////////////////////////////
    // 2. add a menu item with "X" image, which is clicked to quit the program
    //    you may modify it.
    
    // create menu, it's an autorelease object
    auto menu = Menu::create();
    // add a "close" icon to exit the progress. it's an autorelease object
    auto closeItem = MenuItemImage::create(
                                           "CloseNormal.png",
                                           "CloseSelected.png",
                                           CC_CALLBACK_1(LevelLayer::menuCloseCallback, this));
    closeItem->setScale( (visibleSize.height / 8) / closeItem->getContentSize().height );
    closeItem->setPosition(Vec2(origin.x + visibleSize.width/2 - closeItem->getContentSize().width/2 ,
                                origin.y + 2*closeItem->getContentSize().height));
    
    menu->addChild(closeItem);
    
    // add menu items for tests
    auto playLabel = Label::createWithTTF("Play", "fonts/Marker Felt.ttf", 48);
    auto playItem = MenuItemLabel::create( playLabel, CC_CALLBACK_1(LevelLayer::menuPlayCallback, this));
    playItem->setPosition(Vec2( visibleSize.width/2 + origin.x,  visibleSize.height/2 + origin.y));
    menu->addChild(playItem);
    
    //    // add menu items for tests
    //    auto exitLabel = Label::createWithTTF("Exit", "fonts/Marker Felt.ttf", 48);
    //    auto exitItem = MenuItemLabel::create( exitLabel, CC_CALLBACK_1(Main::menuCloseCallback, this));
    //    exitItem->setPosition(Vec2( visibleSize.width/2 + origin.x,  visibleSize.height/2 + origin.y - 2*exitItem->getContentSize().height));
    //    menu->addChild(exitItem);
    
    
    //    this->addChild(playLabel);
    
    menu->setPosition(Vec2::ZERO);
    this->addChild(menu, 1);
    
    
    //    LevelLoader::getInstance()->addFile();
    
    
    return true;
}

void LevelLayer::onEnter()
{
    Layer::onEnter();
}

void LevelLayer::onExit()
{
    
    Layer::onExit();
}

void LevelLayer::cleanup()
{
    Layer::cleanup();
}


void LevelLayer::menuPlayCallback(Ref* pSender)
{
    //    const char* levelName = LevelLoader::getFirstLevel() ;
    //    LevelLoader* loader = LevelLoader::load( levelName );
    
    // create the game scene and run it
    auto scene = GameScene1::create(nullptr);
    
    
    if (scene)
    {
        //        auto fade = TransitionFade::create(3, scene)
        Director::getInstance()->replaceScene(scene);
        //        scene->release();
    }
    
    
}

void LevelLayer::menuCloseCallback(Ref* pSender)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WP8) || (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
    MessageBox("You pressed the close button. Windows Store Apps do not implement a close button.","Alert");
    return;
#endif
    
    
    Director::getInstance()->end();
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
}
