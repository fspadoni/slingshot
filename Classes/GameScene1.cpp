//
//  Scene1.cpp
//  Slingshot
//
//  Created by Federico Spadoni on 12/07/16.
//
//

#include "GameScene1.h"
#include "Main.h"
#include "GameLayer1.h"
#include "LevelLoader.h"
#include "GameConfig.h"

#include "physics-render/CCRenderPass.h"

#include "physics/CCTriangularMesh.h"

#include "physics-render/CCSoftBodyDrawNode.h"

#include "physics-shaders/CCPhysicsShaders.h"


USING_NS_CC;
//USING_NS_CC_EXT;




Scene* GameScene1::create(LevelLoader* loader)
{
    
    if (loader == nullptr)
    {
        //        const char* levelName = LevelLoader::getFirstLevel() ;
        //        loader = LevelLoader::load( levelName );
    }
    
    GameScene1 *scene = new (std::nothrow) GameScene1(loader);
    
    if (scene && scene->init() )
    {
        
        auto layer = GameLayer1::create(loader);
        
        
        layer->setTag(_gameLayerTag);
        
        //    // add layer as a child to scene
        scene->addChild(layer);
        
        scene->autorelease();
        return scene;
    }
    else
    {
        CC_SAFE_DELETE(scene);
        return nullptr;
    }
    
    // return the scene
    return scene;
}


GameScene1::GameScene1(LevelLoader* loader)
: PhysicsScene( GameConfig::getInstance()->_hashCellSize , GameConfig::getInstance()->_hashCellCount)
//, _levelLoader(loader)
, _debugDraw(false)
, buffer(nullptr)
{
    
}


GameScene1::~GameScene1()
{
    //    _levelLoader->release();
}


bool GameScene1::init()
{
    
    if ( !PhysicsScene::initWithPhysics() )
        return false;
    
    
    //    _levelLoader->retain();
    
    //    _physicsWorld->setAutoStep(false);
    _physicsWorld->setSubsteps(4);
    _physicsWorld->setGravity( GameConfig::getInstance()->_gravity );
    
    //    _physicsWorld->setCollisionBias(0.1);
    //    _physicsWorld->setCollisionSlop(0.125);
    _physicsWorld->setIterations(12);
    //    _physicsWorld->setSleepTimeThreshold(100.0/32.0);
    //    _physicsWorld->setIdleSpeedThreshold();
    
    
    auto visibleSize = Director::getInstance()->getVisibleSize();
    
    
    return true;
}


void GameScene1::onEnter()
{
    Node::onEnter();
    
    _threadSafeCallbacks.clear();
    
    _gameLayer = static_cast<GameLayer1*>(getChildByTag(_gameLayerTag) );
    
    
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    MenuItemFont::setFontSize(24);
    auto startItem = MenuItemFont::create("reStart", CC_CALLBACK_1(GameScene1::restartCallback, this));
    auto reStartMenu = Menu::create(startItem, nullptr);
    this->addChild(reStartMenu,64);
    reStartMenu->setPosition(Vec2(origin.x+startItem->getContentSize().width, origin.y+visibleSize.height-startItem->getContentSize().height));
    
    MenuItemFont::setFontSize(24);
    auto stopItem = MenuItemFont::create("stop", CC_CALLBACK_1(GameScene1::mainMenuCallback, this));
    auto stopMenu = Menu::create(stopItem, nullptr);
    this->addChild(stopMenu,64);
    stopMenu->setPosition(Vec2(origin.x+visibleSize.width-startItem->getContentSize().width, origin.y+visibleSize.height-startItem->getContentSize().height));
    
    // menu for debug layer
    MenuItemFont::setFontSize(24);
    auto debugItem = MenuItemFont::create("debug", CC_CALLBACK_1(GameScene1::toggleDebugCallback, this));
    auto debugMenu = Menu::create(debugItem, nullptr);
    this->addChild(debugMenu,64);
    debugMenu->setPosition(Vec2(origin.x+visibleSize.width-debugItem->getContentSize().width, origin.y+debugItem->getContentSize().height));
    
    
    
    auto nextLevelItem = MenuItemImage::create("f1.png", "f2.png", CC_CALLBACK_1(GameScene1::nextLevel, this) );
    auto prevLevelItem = MenuItemImage::create("b1.png", "b2.png", CC_CALLBACK_1(GameScene1::previousLevel, this) );
    
    nextLevelItem->setScale( visibleSize.height / 16 / nextLevelItem->getContentSize().height);
    prevLevelItem->setScale( visibleSize.height / 16 / nextLevelItem->getContentSize().height);
    
    auto levelMenu = Menu::create(nextLevelItem, prevLevelItem, nullptr);
    
    levelMenu->setPosition( origin.x+visibleSize.width/2, origin.y +  nextLevelItem->getContentSize().height);
    nextLevelItem->setPosition( nextLevelItem->getContentSize().width * 1.2 , 0.0 ); //0.0 , VisibleRect::bottom().y+nextLevelItem->getContentSize().height/2);
    prevLevelItem->setPosition( -prevLevelItem->getContentSize().width * 1.2, 0.0 ); //VisibleRect::center().x + prevLevelItem->getContentSize().width*2, VisibleRect::bottom().y+prevLevelItem->getContentSize().height/2);
    
    this->addChild(levelMenu,1);
    
    
    // start ffmpeg telling it to expect raw rgba 720p-60hz frames
    // -i - tells it to read frames from stdin
    //    const char* cmd = "/usr/local/Cellar/ffmpeg/2.4.2/bin/ffmpeg -r 30 -f rawvideo -pix_fmt rgba -s 640x900 -i -  -threads 0 -preset fast -y -pix_fmt yuv420p -crf 21 -vf vflip -r 30 output.mp4"; // 640x960
    //
    ////    const char* getcd
    ////    chdir("/some/where/else");
    //
    //    // open pipe to ffmpeg's stdin in binary write mode
    //    ffmpeg = popen(cmd, "w");
    //
    //    if ( ffmpeg )
    //    {
    //        const int width = Director::getInstance()->getOpenGLView()->getFrameSize().width;
    //        const int height = Director::getInstance()->getOpenGLView()->getFrameSize().height;
    //
    //        buffer = new GLubyte[width * height * 4];
    //    }
    
}


void GameScene1::onEnterTransitionDidFinish()
{
    Node::onEnterTransitionDidFinish();
    
}

//void GameScene1::update(float delta)
//{
//    PhysicsScene::update(delta);
//
//}


void GameScene1::setLevel(LevelLoader* levelLoader)
{
    //    _levelLoader = levelLoader;
    //    _levelLoader->retain();
}


void GameScene1::cleanup()
{
    
    PhysicsScene::cleanup();
    
    //    delete buffer;
    //    buffer = nullptr;
    //    pclose(ffmpeg);
}


bool GameScene1::loadConfig(const std::string& filename)
{
    
}


bool GameScene1::loadLevels(const std::string& filename)
{
    
}




bool GameScene1::doToggleDebug()
{
    _debugDraw = !_debugDraw;
    _physicsWorld->setDebugDrawMask(_debugDraw ? PhysicsWorld::DEBUGDRAW_ALL : PhysicsWorld::DEBUGDRAW_NONE);
    
    _gameLayer->toggleDebugCallback(_debugDraw);
    
    return false;
}


bool GameScene1::doRestart()
{
    
    _gameLayer = static_cast<GameLayer1*>(getChildByTag(_gameLayerTag) );
    _gameLayer->resetBullet();
    return true;
    
    //    Director::getInstance()->pause();
    this->simulationPause();
    
    this->resetPhysicsWorld();
    
    removeChildByTag(_gameLayerTag);

    
    auto layer = GameLayer1::create(nullptr);
    
    
    layer->setTag(_gameLayerTag);
    
//    // add layer as a child to scene
    this->addChild(layer);
    
    return true;
}


bool GameScene1::doMainMenu()
{
    //    Director::getInstance()->pause();
    this->simulationPause();
    
    // create a scene. it's an autorelease object
    auto scene = Main::createScene();
    
    if (scene)
    {
        Director::getInstance()->replaceScene(scene);
    }
    
    return true;
}


bool GameScene1::doNextLevel()
{
    //    Director::getInstance()->pause();
    this->simulationPause();
    
    this->resetPhysicsWorld();
    
    //    auto gameLayer = getChildByTag(_gameLayerTag);
    removeChildByTag(_gameLayerTag);
    
    
    //    const char* nextLevelName = LevelLoader::getNextLevel() ;
    //    _levelLoader->release();
    //    _levelLoader = LevelLoader::load( nextLevelName );
    //    _levelLoader->retain();
    
    auto layer = GameLayer1::create(nullptr);
    //    callLuaFunction<Layer>("scripts/main.lua","onCreateGameLayer", layer, "cc.Layer");
    
    layer->setTag(_gameLayerTag);
    
    //    // add layer as a child to scene
    this->addChild(layer);
    
    _gameLayer = static_cast<GameLayer1*>(getChildByTag(_gameLayerTag) );
    
    return true;
}


bool GameScene1::doPreviousLevel()
{
    //    Director::getInstance()->pause();
    this->simulationPause();
    
    this->resetPhysicsWorld();
    
    //    auto gameLayer = getChildByTag(_gameLayerTag);
    removeChildByTag(_gameLayerTag);
    
    
    //    const char* prevLevelName = LevelLoader::getPreviousLevel() ;
    //    _levelLoader->release();
    //    _levelLoader = LevelLoader::load( prevLevelName );
    //    _levelLoader->retain();
    
    auto layer = GameLayer1::create(nullptr);
    //    callLuaFunction<Layer>("scripts/main.lua","onCreateGameLayer", layer, "cc.Layer");
    
    layer->setTag(_gameLayerTag);
    
    //    // add layer as a child to scene
    this->addChild(layer);
    
    _gameLayer = static_cast<GameLayer1*>(getChildByTag(_gameLayerTag) );
    
    return true;
}


void GameScene1::updateThreadSafe(float delta)
{
    ThreadSafeCallbacksIter lastIter = _threadSafeCallbacks.end();
    for (ThreadSafeCallbacksIter iter = _threadSafeCallbacks.begin(); iter != lastIter; ++iter)
    {
        if ( (*iter)() )
        {
            _threadSafeCallbacks.clear();
            return;
        }
    }
    
    _threadSafeCallbacks.clear();
    
    
    if ( !isSimulationPaused() )
    {
        _gameLayer->updateThreadSafe(delta);
    }
    
    
    // video capture
    //    const int width = Director::getInstance()->getOpenGLView()->getFrameSize().width;
    //    const int height = Director::getInstance()->getOpenGLView()->getFrameSize().height;
    
    
    //    static int captureFrame = 1;
    //
    //    if ( ffmpeg && captureFrame )
    //    {
    //        glPixelStorei(GL_PACK_ALIGNMENT, 1);
    //        glReadPixels(0, 0, width, height, GL_RGBA, GL_UNSIGNED_BYTE, buffer);
    //        fwrite(buffer, width * height * 4, 1, ffmpeg);
    //        captureFrame = 0;
    //    }
    //    else
    //    {
    //        captureFrame = 1;
    //    }
    
    
}
