//
//  RenderTextureLayer.cpp
//  Slingshot
//
//  Created by Federico Spadoni on 11/03/16.
//
//

#include "RenderTextureLayer.h"

using namespace cocos2d;

RenderTextureLayer* RenderTextureLayer::create(const std::string& vertexShaderFile, const std::string& fragmentShaderFile)
{
	RenderTextureLayer* p = new (std::nothrow) RenderTextureLayer();
	if (p && p->init(vertexShaderFile, fragmentShaderFile)) {
		p->autorelease();
		return p;
	}
	else {
		CC_SAFE_DELETE(p);
		return nullptr;
	}
}


RenderTextureLayer* RenderTextureLayer::createWithByteArrays(const GLchar* vShaderByteArray, const GLchar* fShaderByteArray)
{
	RenderTextureLayer* p = new (std::nothrow) RenderTextureLayer();
	if (p && p->init(vShaderByteArray, fShaderByteArray)) {
		p->autorelease();
		return p;
	}
	else {
		CC_SAFE_DELETE(p);
		return nullptr;
	}
}


RenderTextureLayer::RenderTextureLayer()
: _renderTexture(nullptr)
{
    
}

RenderTextureLayer::~RenderTextureLayer()
{
	_renderTexture->release();
}

bool RenderTextureLayer::init(const std::string& vertexShaderFile, const std::string& fragmentShaderFile)
{
//	if (!Layer::init())
//    {
//		return false;
//	}
    
	GLProgram* glProgram = GLProgram::createWithFilenames(vertexShaderFile, fragmentShaderFile);
//	m_pimpl->glProgram->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_COLOR, GLProgram::VERTEX_ATTRIB_POSITION);
//	m_pimpl->glProgram->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_POSITION, GLProgram::VERTEX_ATTRIB_COLOR);
//	m_pimpl->glProgram->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_TEX_COORD, GLProgram::VERTEX_ATTRIB_TEX_COORD);
//	m_pimpl->glProgram->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_TEX_COORD1, GLProgram::VERTEX_ATTRIB_TEX_COORD1);
//	m_pimpl->glProgram->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_TEX_COORD2, GLProgram::VERTEX_ATTRIB_TEX_COORD2);
//	m_pimpl->glProgram->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_TEX_COORD3, GLProgram::VERTEX_ATTRIB_TEX_COORD3);
//	m_pimpl->glProgram->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_NORMAL, GLProgram::VERTEX_ATTRIB_NORMAL);
//	m_pimpl->glProgram->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_BLEND_WEIGHT, GLProgram::VERTEX_ATTRIB_BLEND_WEIGHT);
//	m_pimpl->glProgram->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_BLEND_INDEX, GLProgram::VERTEX_ATTRIB_BLEND_INDEX);
//	m_pimpl->glProgram->link();
//	m_pimpl->glProgram->updateUniforms();
    
//    if ( glProgram && init(glProgram) )
//        return true;
    
    return false;
}

bool RenderTextureLayer::init(const GLchar* vShaderByteArray, const GLchar* fShaderByteArray)
{
//    if (!Layer::init())
//    {
//		return false;
//	}
    
    GLProgram* glProgram = GLProgram::createWithByteArrays(vShaderByteArray, fShaderByteArray);

//    if ( glProgram && init(glProgram) )
//        return true;
    
    return false;
}

bool RenderTextureLayer::init()
{
    if (!Layer::init())
    {
		return false;
	}
    
//    auto glProgramState = GLProgramState::getOrCreateWithGLProgram(glProgram);
//    setGLProgramState(glProgramState);
    
    auto visibleSize = Director::getInstance()->getVisibleSize();
    
	_renderTexture = RenderTexture::create(visibleSize.width, visibleSize.height, Texture2D::PixelFormat::RGBA8888);
	_renderTexture->retain();
    
//	_sprite = Sprite::createWithTexture( _renderTexture->getSprite()->getTexture() );
//	_sprite->setTextureRect(Rect(0, 0, _sprite->getTexture()->getContentSize().width,
//                                 _sprite->getTexture()->getContentSize().height));
//	_sprite->setAnchorPoint(Point::ZERO);
//	_sprite->setPosition(Point::ZERO);
//	_sprite->setFlippedY(true);
//	_sprite->setGLProgram( getGLProgram() );
//	this->addChild(_sprite);
    
	return true;

}

//void RenderTextureLayer::draw(cocos2d::Renderer* renderer, const cocos2d::Mat4 &transform, uint32_t flags)
//{
//    _renderTexture->begin();
//    Layer::draw( renderer, transform, flags);
//    _renderTexture->end();
//}

void RenderTextureLayer::visit(cocos2d::Renderer* renderer, const cocos2d::Mat4 &parentTransform, uint32_t parentFlags)
{
    _renderTexture->beginWithClear(0.0f, 0.0f, 0.0f, 0.0f);
    
    Layer::visit( renderer, parentTransform, parentFlags);
    
    _renderTexture->end();
}

//void RenderTextureLayer::draw(RenderTextureLayer* layer)
//{
//	_renderTexture->beginWithClear(0.0f, 0.0f, 0.0f, 0.0f);

//    RenderTexture* otherRenderTexture = layer->getRenderTexture();
//    Texture2D* inputTexture = layer->getTexture();
//	layer->visit();
    
	// In case you decide to replace Layer* with Node*,
	// since some 'Node' derived classes do not have visit()
	// member function without an argument:
	//auto renderer = Director::getInstance()->getRenderer();
	//auto& parentTransform = Director::getInstance()->getMatrix(MATRIX_STACK_TYPE::MATRIX_STACK_MODELVIEW);
	//layer->visit(renderer, parentTransform, true);
    
//	_renderTexture->end();
//    
//	_sprite->setTexture( _renderTexture->getSprite()->getTexture());
//}
