//
//  RenderPass.cpp
//  Slingshot
//
//  Created by Federico Spadoni on 11/03/16.
//
//

#include "RenderPass.h"

using namespace cocos2d;



RenderPass* RenderPass::create(cocos2d::Texture2D* inputTexture)
{
	RenderPass* p = new (std::nothrow) RenderPass();
	if (p && p->init(inputTexture) ) {
		p->autorelease();
		return p;
	}
	else {
		CC_SAFE_DELETE(p);
		return nullptr;
	}
}

RenderPass* RenderPass::create()
{
    RenderPass* p = new (std::nothrow) RenderPass();
	if (p && p->init() ) {
		p->autorelease();
		return p;
	}
	else {
		CC_SAFE_DELETE(p);
		return nullptr;
	}
}



RenderPass::RenderPass()
: Sprite()
, _renderTexture(nullptr)
//, _inputSprite(nullptr)
{
    
}

RenderPass::~RenderPass()
{
	_renderTexture->release();
//    _inputSprite->release();
}

//bool RenderPass::init(cocos2d::Texture2D* inputTexture, const std::string& vertexShaderFile, const std::string& fragmentShaderFile)
//{

    
//	GLProgram* glProgram = GLProgram::createWithFilenames(vertexShaderFile, fragmentShaderFile);
    //	m_pimpl->glProgram->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_COLOR, GLProgram::VERTEX_ATTRIB_POSITION);
    //	m_pimpl->glProgram->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_POSITION, GLProgram::VERTEX_ATTRIB_COLOR);
    //	m_pimpl->glProgram->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_TEX_COORD, GLProgram::VERTEX_ATTRIB_TEX_COORD);
    //	m_pimpl->glProgram->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_TEX_COORD1, GLProgram::VERTEX_ATTRIB_TEX_COORD1);
    //	m_pimpl->glProgram->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_TEX_COORD2, GLProgram::VERTEX_ATTRIB_TEX_COORD2);
    //	m_pimpl->glProgram->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_TEX_COORD3, GLProgram::VERTEX_ATTRIB_TEX_COORD3);
    //	m_pimpl->glProgram->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_NORMAL, GLProgram::VERTEX_ATTRIB_NORMAL);
    //	m_pimpl->glProgram->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_BLEND_WEIGHT, GLProgram::VERTEX_ATTRIB_BLEND_WEIGHT);
    //	m_pimpl->glProgram->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_BLEND_INDEX, GLProgram::VERTEX_ATTRIB_BLEND_INDEX);
    //	m_pimpl->glProgram->link();
    //	m_pimpl->glProgram->updateUniforms();
    
//    if ( glProgram && init(inputTexture, glProgram) )
//        return true;
//    
//    return false;
//}

bool RenderPass::init()
{
    return Sprite::init();
}


bool RenderPass::init(cocos2d::Texture2D* inputTexture)
{

    if ( !Sprite::initWithTexture(inputTexture) )
        return false;
    
    setAnchorPoint(Point::ZERO);
    setPosition(Point::ZERO);
    setFlippedY(true);
    
    _renderTexture = RenderTexture::create(getTexture()->getContentSize().width, getTexture()->getContentSize().height, Texture2D::PixelFormat::RGBA8888);
    
    if ( _renderTexture == nullptr)
        return false;
    
	_renderTexture->retain();
    
//    GLProgram* glProgram = GLProgram::createWithByteArrays(vShaderByteArray, fShaderByteArray);
    
    //    if ( glProgram && init(glProgram) )
    //        return true;
    
    return true;
}


void RenderPass::setOutputTextureSize(int width, int height)
{

    RenderTexture* renderTexture = RenderTexture::create(width, height, Texture2D::PixelFormat::RGBA8888);
    
    if ( renderTexture != nullptr )
    {
        CC_SAFE_RETAIN(renderTexture);
        CC_SAFE_RELEASE(_renderTexture);
        _renderTexture = renderTexture;
    }
    
    setAnchorPoint(Point::ZERO);
    setPosition(Point::ZERO);
    setFlippedY(true);
    
    if ( Sprite::getContentSize().width > 0 && Sprite::getContentSize().height > 0)
    {
        Sprite::setScale( _renderTexture->getSprite()->getContentSize().width / getContentSize().width, _renderTexture->getSprite()->getContentSize().height / getContentSize().height);
    }
    
}


void RenderPass::setInputTexture(cocos2d::Texture2D* inputTexture, bool resizeOutputTexture)
{
    
    Sprite::setTexture(inputTexture);
    
    Rect rect = Rect::ZERO;
    rect.size = inputTexture->getContentSize();
    
    Sprite::setTextureRect(rect);
    
//    getTexture()->generateMipmap();
    getTexture()->setAntiAliasTexParameters();
    
    if ( resizeOutputTexture )
    {
        RenderTexture* renderTexture = RenderTexture::create(getTexture()->getContentSize().width, getTexture()->getContentSize().height, Texture2D::PixelFormat::RGBA8888);
        
        if ( renderTexture != nullptr )
        {
            CC_SAFE_RETAIN(renderTexture);
            CC_SAFE_RELEASE(_renderTexture);
            _renderTexture = renderTexture;
        }

//        Texture2D::TexParams    texParams;
//        texParams.minFilter = GL_LINEAR;
//        texParams.magFilter = GL_LINEAR;
//        texParams.wrapS = GL_REPEAT;
//        texParams.wrapT = GL_REPEAT;
//        renderTexture->getSprite()->getTexture()->setTexParameters(texParams);
        
        setAnchorPoint(Point::ZERO);
        setPosition(Point::ZERO);
        setFlippedY(true);
    }
    
    else if ( _renderTexture->getSprite()->getContentSize().width > 0 && _renderTexture->getSprite()->getContentSize().height > 0)
    {
//        _renderTexture->getSprite()->setScale(Sprite::getContentSize().width /  _renderTexture->getSprite()->getContentSize().width, Sprite::getContentSize().height / _renderTexture->getSprite()->getContentSize().height);
        Sprite::setScale( _renderTexture->getSprite()->getContentSize().width / Sprite::getContentSize().width, _renderTexture->getSprite()->getContentSize().height / Sprite::getContentSize().height);
        
    }
}


void RenderPass::setGLShader(const std::string& vertexShaderFile, const std::string& fragmentShaderFile)
{
    GLProgram* glProgram = GLProgram::createWithFilenames(vertexShaderFile, fragmentShaderFile);
    auto glProgramState = GLProgramState::getOrCreateWithGLProgram(glProgram);
    setGLProgramState(glProgramState);
}

void RenderPass::setGLShader(const GLchar* vShaderByteArray, const GLchar* fShaderByteArray)
{
    
    GLProgram* glProgram = GLProgram::createWithByteArrays(vShaderByteArray, fShaderByteArray);
    auto glProgramState = GLProgramState::getOrCreateWithGLProgram(glProgram);
    setGLProgramState(glProgramState);
}




void RenderPass::visit(cocos2d::Renderer* renderer, const cocos2d::Mat4 &parentTransform, uint32_t parentFlags)
{
    _renderTexture->beginWithClear(0.0f, 0.0f, 0.0f, 0.0f);
    
    Node::visit( renderer, parentTransform, parentFlags);
    
    _renderTexture->end();
}

//bool RenderPass::init(cocos2d::Texture2D* inputTexture, cocos2d::GLProgram* glProgram)
//{
//    if (!Layer::init())
//    {
//		return false;
//	}

    //    auto glProgramState = GLProgramState::getOrCreateWithGLProgram(glProgram);
    //    setGLProgramState(glProgramState);
    
//    auto visibleSize = Director::getInstance()->getVisibleSize();

//	_renderTexture = RenderTexture::create(visibleSize.width, visibleSize.height);
//	_renderTexture->retain();

//    _inputSprite = Sprite::createWithTexture( inputTexture );
//    _inputSprite->retain();
    
    //	_sprite = Sprite::createWithTexture( _renderTexture->getSprite()->getTexture() );
    //	_sprite->setTextureRect(Rect(0, 0, _sprite->getTexture()->getContentSize().width,
    //                                 _sprite->getTexture()->getContentSize().height));
    //	_sprite->setAnchorPoint(Point::ZERO);
    //	_sprite->setPosition(Point::ZERO);
    //	_sprite->setFlippedY(true);
    //	_sprite->setGLProgram( getGLProgram() );
    //	this->addChild(_sprite);
    
//	return true;
//    
//}

//void RenderPass::draw(cocos2d::Renderer* renderer, const cocos2d::Mat4 &transform, uint32_t flags)
//{
//    _renderTexture->begin();
//    Layer::draw( renderer, transform, flags);
//    _renderTexture->end();
//}
//
//void RenderPass::visit(cocos2d::Renderer* renderer, const cocos2d::Mat4 &parentTransform, uint32_t parentFlags)
//{
//    _renderTexture->beginWithClear(0.0f, 0.0f, 0.0f, 0.0f);
//    
//    Layer::visit( renderer, parentTransform, parentFlags);
//    
//    _renderTexture->end();
//}

//void RenderPass::draw(RenderPass* layer)
//{
//	_renderTexture->beginWithClear(0.0f, 0.0f, 0.0f, 0.0f);

//    RenderTexture* otherRenderTexture = layer->getRenderTexture();
//    Texture2D* inputTexture = layer->getTexture();
//	layer->visit();

// In case you decide to replace Layer* with Node*,
// since some 'Node' derived classes do not have visit()
// member function without an argument:
//auto renderer = Director::getInstance()->getRenderer();
//auto& parentTransform = Director::getInstance()->getMatrix(MATRIX_STACK_TYPE::MATRIX_STACK_MODELVIEW);
//layer->visit(renderer, parentTransform, true);

//	_renderTexture->end();
//    
//	_sprite->setTexture( _renderTexture->getSprite()->getTexture());
//}



SpriteRenderPass* SpriteRenderPass::create(int width, int height, cocos2d::Texture2D::PixelFormat eFormat)
{
	SpriteRenderPass* p = new (std::nothrow) SpriteRenderPass();
	if (p && p->init(width, height, eFormat)) {
		p->autorelease();
		return p;
	}
	else {
		CC_SAFE_DELETE(p);
		return nullptr;
	}
}


SpriteRenderPass* SpriteRenderPass::create(cocos2d::Texture2D* texture)
{
	SpriteRenderPass* p = new (std::nothrow) SpriteRenderPass();
	if (p && p->initWithTexture(texture)) {
        p->setAnchorPoint(Point::ZERO);
        p->setPosition(Point::ZERO);
        p->setFlippedY(true);
		p->autorelease();
		return p;
	}
	else {
		CC_SAFE_DELETE(p);
		return nullptr;
	}
}

SpriteRenderPass::SpriteRenderPass()
: Sprite()
{
    
}

SpriteRenderPass::~SpriteRenderPass()
{
    glDeleteBuffers(2, _vbo);

    if (Configuration::getInstance()->supportsShareableVAO())
    {
        GL::bindVAO(0);
        glDeleteVertexArrays(1, &_vao);
    }
}


bool SpriteRenderPass::init(int width, int height, cocos2d::Texture2D::PixelFormat format)
{
    
    Texture2D* texture = createTexture(width, height, format);
    
    if ( texture == nullptr )
        return false;
    
    if ( !Sprite::initWithTexture(texture) )
        return false;
    
    setAnchorPoint(Point::ZERO);
    setPosition(Point::ZERO);
    setFlippedY(true);
    

    setupBuffers();
    
    return true;
}

void SpriteRenderPass::addTexture(cocos2d::Texture2D* texture, const unsigned int num)
{
    CCASSERT( num < 4, "only 4 textures are suppoerted" );
    
    GLint samplerValueLocation;
    
    if (num == 0)
    {
        samplerValueLocation = _glProgramState->getGLProgram()->getUniformLocation(GLProgram::UNIFORM_NAME_SAMPLER0);
    }
    else if (num == 1)
    {
        samplerValueLocation = _glProgramState->getGLProgram()->getUniformLocation(GLProgram::UNIFORM_NAME_SAMPLER1);
    }
    else if (num == 2)
    {
        samplerValueLocation = _glProgramState->getGLProgram()->getUniformLocation(GLProgram::UNIFORM_NAME_SAMPLER2);
    }
    else if (num == 3)
    {
        samplerValueLocation = _glProgramState->getGLProgram()->getUniformLocation(GLProgram::UNIFORM_NAME_SAMPLER3);
    }
    
    if (samplerValueLocation < 0 )
        return;
    

    _glProgramState->getGLProgram()->use();
    _glProgramState->getGLProgram()->setUniformLocationWith1i(samplerValueLocation, num);
    
    _inputTextures.insert(num, texture);
    
    
//    texture->generateMipmap();
//    texture->setAntiAliasTexParameters();

    
//        _inputTextures.insert(GLProgram::UNIFORM_NAME_SAMPLER0, texture);
//    else if (num == 1)
//        _inputTextures.insert(GLProgram::UNIFORM_NAME_SAMPLER1, texture);
//    else if (num == 2)
//        _inputTextures.insert(GLProgram::UNIFORM_NAME_SAMPLER2, texture);
//    else if (num == 3)
//        _inputTextures.insert(GLProgram::UNIFORM_NAME_SAMPLER3, texture);
//
//    
//    for ( auto tex : _inputTextures )
//    {
//        GLint samplerValueLocation = _glProgramState->getGLProgram()->getUniformLocation(tex.first);
//        _glProgramState->getGLProgram()->use();
//        _glProgramState->getGLProgram()->setUniformLocationWith1i(samplerValueLocation, num); //tex.second->getName() );
//        
//    }
    
//    _glProgramState->getGLProgram()->link();
//    _glProgramState->getGLProgram()->updateUniforms();

}

void SpriteRenderPass::setGLShader(const std::string& vertexShaderFile, const std::string& fragmentShaderFile)
{
    GLProgram* glProgram = GLProgram::createWithFilenames(vertexShaderFile, fragmentShaderFile);
    auto glProgramState = GLProgramState::getOrCreateWithGLProgram(glProgram);
    setGLProgramState(glProgramState);
}

void SpriteRenderPass::setGLShader(const GLchar* vShaderByteArray, const GLchar* fShaderByteArray)
{
    
    GLProgram* glProgram = GLProgram::createWithByteArrays(vShaderByteArray, fShaderByteArray);
    auto glProgramState = GLProgramState::getOrCreateWithGLProgram(glProgram);
    setGLProgramState(glProgramState);
}


Texture2D* SpriteRenderPass::createTexture(int w, int h, cocos2d::Texture2D::PixelFormat format)
{
    CCASSERT(format != Texture2D::PixelFormat::A8, "only RGB and RGBA formats are valid for a render texture");
    
    void *data = nullptr;
    
    //Size size = Director::getInstance()->getWinSizeInPixels();
    //_fullviewPort = Rect(0,0,size.width,size.height);
    w = (int)(w * CC_CONTENT_SCALE_FACTOR());
    h = (int)(h * CC_CONTENT_SCALE_FACTOR());
    
    // textures must be power of two squared
    int powW = 0;
    int powH = 0;
    
    if (Configuration::getInstance()->supportsNPOT())
    {
        powW = w;
        powH = h;
    }
    else
    {
        powW = ccNextPOT(w);
        powH = ccNextPOT(h);
    }
    
    auto dataLen = powW * powH * 4;
    data = malloc(dataLen);
    if (data == nullptr)
        return nullptr;
    
    memset(data, 0, dataLen);
    
    auto texture = new (std::nothrow) Texture2D();
    if (texture)
    {
        texture->initWithData(data, dataLen, (Texture2D::PixelFormat)format, powW, powH, Size((float)w, (float)h));
    }
    
//    Texture2D::TexParams texParams;
//    texParams.minFilter = GL_LINEAR;
//    texParams.magFilter = GL_LINEAR;
//    texture->setTexParameters(texParams);
    
//    texture->generateMipmap()
//    texture->setAntiAliasTexParameters();
    
    // retained
    //        setSprite(Sprite::createWithTexture(_texture));
    
    return texture;
    
}

void SpriteRenderPass::setupBuffers()
{
    
    _quadVerts[0].vertices = _quad.tl.vertices;
    _quadVerts[0].texCoords = _quad.tl.texCoords;
    _quadVerts[1].vertices = _quad.bl.vertices;
    _quadVerts[1].texCoords = _quad.bl.texCoords;
    _quadVerts[2].vertices = _quad.tr.vertices;
    _quadVerts[2].texCoords = _quad.tr.texCoords;
    _quadVerts[3].vertices = _quad.br.vertices;
    _quadVerts[3].texCoords = _quad.br.texCoords;
    
    _quadIndices[0] = (GLushort) 0;
    _quadIndices[1] = (GLushort) 1;
    _quadIndices[2] = (GLushort) 2;
    _quadIndices[3] = (GLushort) 3;
    _quadIndices[4] = (GLushort) 2;
    _quadIndices[5] = (GLushort) 1;
    
    
    if (Configuration::getInstance()->supportsShareableVAO())
    {
        glGenVertexArrays(1, &_vao);
        GL::bindVAO(_vao);
    }

    
    glGenBuffers(2, &_vbo[0]);
    
    glBindBuffer(GL_ARRAY_BUFFER, _vbo[0]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(V3F_T2F)*4, _quadVerts, GL_STATIC_DRAW);
    
    // vertices
    glEnableVertexAttribArray(GLProgram::VERTEX_ATTRIB_POSITION);
    glVertexAttribPointer(GLProgram::VERTEX_ATTRIB_POSITION, 3, GL_FLOAT, GL_FALSE, sizeof(V3F_T2F), (GLvoid*) offsetof( V3F_T2F, vertices));
    
    // tex coords
    glEnableVertexAttribArray(GLProgram::VERTEX_ATTRIB_TEX_COORD);
    glVertexAttribPointer(GLProgram::VERTEX_ATTRIB_TEX_COORD, 2, GL_FLOAT, GL_FALSE, sizeof(V3F_T2F), (GLvoid*) offsetof( V3F_T2F, texCoords));
    
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _vbo[1]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLushort) * 6, _quadIndices, GL_STATIC_DRAW);
    

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    if (Configuration::getInstance()->supportsShareableVAO())
    {
        GL::bindVAO(0);
    }
    
    CHECK_GL_ERROR_DEBUG();
    
}

void SpriteRenderPass::transformQuad(const cocos2d::Mat4 &modelView)
{
//    const Mat4& modelView = cmd->getModelView();
    modelView.transformPoint( &_quadVerts[0].vertices );
    modelView.transformPoint( &_quadVerts[1].vertices );
    modelView.transformPoint( &_quadVerts[2].vertices );
    modelView.transformPoint( &_quadVerts[3].vertices );
}


void SpriteRenderPass::visit(cocos2d::Renderer* renderer, const cocos2d::Mat4 &parentTransform, uint32_t parentFlags)
{
    // quick return if not visible. children won't be drawn.
    if (!_visible)
    {
        return;
    }
    
    uint32_t flags = processParentFlags(parentTransform, parentFlags);
    
    // IMPORTANT:
    // To ease the migration to v3.0, we still support the Mat4 stack,
    // but it is deprecated and your code should not rely on it
    _director->pushMatrix(MATRIX_STACK_TYPE::MATRIX_STACK_MODELVIEW);
    _director->loadMatrix(MATRIX_STACK_TYPE::MATRIX_STACK_MODELVIEW, _modelViewTransform);
    
    bool visibleByCamera = isVisitableByVisitingCamera();
    

    if (visibleByCamera)
    {
        this->draw(renderer, _modelViewTransform, flags);
    }
    
    _director->popMatrix(MATRIX_STACK_TYPE::MATRIX_STACK_MODELVIEW);
    
    // FIX ME: Why need to set _orderOfArrival to 0??
    // Please refer to https://github.com/cocos2d/cocos2d-x/pull/6920
    // reset for next frame
    // _orderOfArrival = 0;
}

void SpriteRenderPass::draw(cocos2d::Renderer *renderer, const cocos2d::Mat4 &transform, uint32_t flags)
{
    _customCommand.init(_globalZOrder, transform, flags);
    _customCommand.func = CC_CALLBACK_0(SpriteRenderPass::onDraw, this, transform, flags);
    renderer->addCommand(&_customCommand);
    
//    Sprite::draw(renderer, transform, flags);
}

void SpriteRenderPass::onDraw(const cocos2d::Mat4 &transform, uint32_t flags)
{
    auto glProgram = getGLProgram();
    glProgram->use();
//    glProgram->setUniformsForBuiltins(_modelViewTransform);
    
    _glProgramState->apply(transform);
    
 
    for ( auto tex : _inputTextures )
    {
        cocos2d::GL::bindTexture2DN( tex.first, tex.second->getName());
    }
    
    
    GL::blendFunc(_blendFunc.src, _blendFunc.dst);
    

    
    if (Configuration::getInstance()->supportsShareableVAO())
    {
        GL::bindVAO(_vao);
    }
    else
    {
        GL::enableVertexAttribs(GL::VERTEX_ATTRIB_FLAG_POSITION | GL::VERTEX_ATTRIB_FLAG_TEX_COORD);
        
        glBindBuffer(GL_ARRAY_BUFFER, _vbo[0]);
        // vertex
        glVertexAttribPointer(GLProgram::VERTEX_ATTRIB_POSITION, 3, GL_FLOAT, GL_FALSE, sizeof(V3F_T2F), (GLvoid *)offsetof(V3F_T2F, vertices));
        
        // texcood
        glVertexAttribPointer(GLProgram::VERTEX_ATTRIB_TEX_COORD, 2, GL_FLOAT, GL_FALSE, sizeof(V3F_T2F), (GLvoid *)offsetof(V3F_T2F, texCoords));

        
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _vbo[1]);

    }
    

    glDrawElements(GL_TRIANGLES, (GLsizei) 6, GL_UNSIGNED_SHORT, _quadIndices );

    

    CC_INCREMENT_GL_DRAWS(1);
    CHECK_GL_ERROR_DEBUG();
    
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    
    if (Configuration::getInstance()->supportsShareableVAO())
    {
        //Unbind VAO
        GL::bindVAO(0);
    }


}
