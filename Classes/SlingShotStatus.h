//
//  SlingShotStatus.h
//  Slingshot
//
//  Created by Federico Spadoni on 24/01/16.
//
//

#ifndef __Slingshot__SlingShotStatus__
#define __Slingshot__SlingShotStatus__


class SlingShot;


class SlingShotStatus
{
public:
    
	virtual ~SlingShotStatus() {}
    
    
	virtual void enter(SlingShot*) = 0;
    
	virtual void execute(SlingShot*) = 0;
    
	virtual void exit(SlingShot*) = 0;
};


class ActiveStatus : public SlingShotStatus
{
    
public:
    
    virtual ~ActiveStatus() {}
    
	virtual void enter(SlingShot*)
	{
	}
    
	virtual void execute(SlingShot*)
	{
	}
    
	virtual void exit(SlingShot*)
	{
	}
    
};

class DrawingStatus : public SlingShotStatus
{
    
public:
    
    virtual ~DrawingStatus() {}
    
	virtual void enter(SlingShot*)
	{
	}
    
	virtual void execute(SlingShot*)
	{
	}
    
	virtual void exit(SlingShot*)
	{
	}
    
};


//class ShootingStatus : public SlingShotStatus
//{
//    
//public:
//    
//    virtual ~ShootingStatus() {}
//    
//	virtual void enter(SlingShot*)
//	{
//	}
//    
//	virtual void execute(SlingShot*)
//	{
//	}
//    
//	virtual void exit(SlingShot*)
//	{
//	}
//    
//};


#endif /* defined(__Slingshot__SlingShotStatus__) */
