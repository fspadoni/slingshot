//
//  Asteroid.h
//  Slingshot
//
//  Created by Federico Spadoni on 01/03/16.
//
//

#ifndef __Slingshot__Asteroid__
#define __Slingshot__Asteroid__

#include "cocos2d.h"
#include "physics/CCPhysicsSoftBody.h"



NS_CC_BEGIN
//class GameLayer;
NS_CC_END


class Asteroid : public cocos2d::PhysicsSoftBody
{
public:
    
    static Asteroid* create(const std::string& triangularMeshFile, const std::string&  materialFile, const float scale);
    
    
    
//    inline void setLayer(cocos2d::GameLayer* layer) { _gameLayer = layer; }
    
    
private:
    
    Asteroid();
    virtual ~Asteroid();
    
    virtual bool init(const std::string& triangularMeshFile, const std::string&  materialFile, const float scale);
    
    
//    cocos2d::GameLayer* _gameLayer;
    
};



#endif /* defined(__Slingshot__Asteroid__) */
