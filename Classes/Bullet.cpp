//
//  Bullet.cpp
//  Slingshot
//
//  Created by Federico Spadoni on 18/02/16.
//
//

#include "Bullet.h"

#include "SlingShot.h"
#include "GameConfig.h"
#include "GameScene.h"

#include "2d/CCSprite.h"
#include "physics/CCPhysicsRigidShape.h"
#include "physics-shaders/CCPhysicsShaders.h"


#include "chipmunk.h"

USING_NS_CC;

FiredStatus Bullet::Fired;
ChargedStatus Bullet::Charged;
//ShootingStatus SlingShot::Shooting;


Bullet* Bullet::create(float radius, float mass, float moment, float elasticity, float friction)
{
    Bullet* bullet = new (std::nothrow) Bullet();
    
    if (bullet && bullet->init(radius, mass, moment, elasticity, friction) )
    {
        bullet->autorelease();
    }
    else{
        CC_SAFE_RELEASE(bullet);
        return nullptr;
    }
    
    return bullet;
}


//Bullet* Bullet::clone(const Bullet* other)
//{
//    Bullet* bullet = new (std::nothrow) Bullet();
//    
//    if (bullet && bullet->copy(other) )
//    {
//        bullet->autorelease();
//    }
//    else{
//        CC_SAFE_RELEASE(bullet);
//        return nullptr;
//    }
//    
//    return bullet;
//}


Bullet::Bullet()
: PhysicsRigidBody()
, _currentStatus(&Fired)
, _contactCounter(0)
, _radius(4.0)
{
    
}

Bullet::~Bullet()
{
    _bulletSprite->release();
}

bool Bullet::init(float radius, float mass, float moment, float elasticity, float friction)
{
    do
    {
        PhysicsRigidBody::init(mass, moment);
        
        addShape(PhysicsRigidShape::createCircle(radius, elasticity, friction));
        
        PhysicsRigidBody::notifyBeginCollision(false);
        PhysicsRigidBody::notifySeparateCollision(false);
        
        
        _bulletSprite = Sprite::create("textures/bulletFireball.png");
        _bulletSprite = Sprite::create("textures/bullets/planetBurns5.png");
        _bulletSprite->retain();
        float scaleX = GameConfig::getInstance()->_bulletSize / _bulletSprite->getContentSize().width;
        float scaleY = GameConfig::getInstance()->_bulletSize / _bulletSprite->getContentSize().height;
        _bulletSprite->setScale(scaleX, scaleY );
        _bulletSprite->setColor(Color3B::RED);
        
        auto  _glShaderProgram = GLProgram::createWithByteArrays(cocos2d::ccShader_2D_Bullet_vert, cocos2d::ccShader_2D_Bullet_frag  );
        auto glProgramState = GLProgramState::getOrCreateWithGLProgram(_glShaderProgram);
//        glProgramState->setUniformFloatv("radius", 1, &_beamThickness);
        _bulletSprite->setGLProgramState(glProgramState);

        
        return true;
    } while (false);
    
    return false;

}


void Bullet::setSlingShot(const SlingShot* slingShot)
{
    _slingShot = slingShot;
}


bool Bullet::checkForStatus(const cocos2d::PhysicsWorld* physicsWorld)
{
    
    cpPointQueryInfo info;
    
    float maxDistance = cpCircleShapeGetRadius( *getCPShapes().begin() );
    
    if (_currentStatus == &Bullet::Charged)
    {
        maxDistance = cpCircleShapeGetRadius( *getCPShapes().begin() ) + 32.0;
    }

    const cpVect position = cpBodyGetPosition( getCPBody() );
    
    const cpShapeFilter filter = {CP_NO_GROUP, CollisionCategory::slingshotNodes | CollisionCategory::slingshotSegments, CollisionCategory::slingshotNodes | CollisionCategory::slingshotSegments };
    
    if ( cpShape* shape = cpSpacePointQueryNearest( cpGetSpace(physicsWorld->getCPspace()),
                                              position,
                                              maxDistance,
                                              filter,
                                              &info) )
    {
//        cpShapeGetBody(shape);
        if ( cpShapeGetUserData(shape) == _slingShot->getBeamBody() )
        {
                if (_currentStatus == &Bullet::Fired)
                {
                    changeStatus( &Bullet::Charged );
                    updateStatus();
                }
        }
        
    }
    else
    {
        if (_currentStatus == &Bullet::Charged)
        {
            changeStatus( &Bullet::Fired );
            updateStatus();
        }
    }
        
    
}


void Bullet::collisionBegin( PhysicsRigidBody* collidingBody, cpArbiter* arb )
{
    CP_ARBITER_GET_SHAPES(arb, shapeA, shapeB );
    CP_ARBITER_GET_BODIES(arb, hook, crate);

        
}


void Bullet::collisionSeparate(PhysicsRigidBody* collidingBody, cpArbiter* arb)
{
    
}

void Bullet::changeStatus(BulletStatus* newStatus)
{
    _currentStatus->exit(this);
    
    _currentStatus = newStatus;
    
    _currentStatus->enter(this);
}

void Bullet::updateStatus()
{
    if ( _currentStatus )
    {
        _currentStatus->execute(this);
    }
}

