//
//  Asteroid.cpp
//  Slingshot
//
//  Created by Federico Spadoni on 01/03/16.
//
//

#include "Asteroid.h"

#include "GameScene.h"

#include "physics/CCMaterialProperty.h"

#include "GameConfig.h"

using namespace cocos2d;



Asteroid* Asteroid::create(const std::string& triangularMeshFile, const std::string&  materialFile, const float scale)
{
    Asteroid* asteroid = new (std::nothrow) Asteroid();
    
    if (asteroid && asteroid->init(triangularMeshFile, materialFile, scale) )
    {
        asteroid->autorelease();
    }
    else{
        CC_SAFE_RELEASE(asteroid);
        return nullptr;
    }
    
    return asteroid;
}


Asteroid::Asteroid()
: PhysicsSoftBody()
{
    
}

Asteroid::~Asteroid()
{
    
}


bool Asteroid::init(const std::string& triangularMeshFile, const std::string&  materialFile, const float scale)
{
    std::string asteroidFullpath = triangularMeshFile;
    if (asteroidFullpath.size() == 0)
        return false;
        
    auto mesh = TriangularMesh::createFromFile(asteroidFullpath);
    if (mesh == nullptr)
        return false;
    
    mesh->setScale(scale);
    
    std::string materialFullpath = FileUtils::getInstance()->fullPathForFilename(materialFile);
    
    if ( materialFullpath.size() == 0)
        return false;
    
    auto asteroidMaterial = SoftBodyMaterial::createFromFile(materialFullpath);
    if (asteroidMaterial == nullptr)
        return false;
    
    if ( PhysicsSoftBody::init(mesh, asteroidMaterial) == false )
        return false;
    
    setCollisionFilter(CollisionCategory::actor);
    
    return true;
    
}



