//
//  GameConfig.cpp
//  game
//
//  Created by Federico Spadoni on 24/02/15.
//
//

#include "GameConfig.h"



USING_NS_CC;


GameConfig* GameConfig::s_singleInstance = nullptr;


GameConfig* GameConfig::getInstance()
{
    if (s_singleInstance == nullptr)
    {
        s_singleInstance = new (std::nothrow) GameConfig();
    }
    return s_singleInstance;
}


void GameConfig::destroyInstance()
{
    CC_SAFE_RELEASE_NULL(s_singleInstance);
}


GameConfig::GameConfig()
: _designSize( Size(480, 720) ) //Size(640, 960) )
, _gravity( cocos2d::Vect(0.0, -980) )
, _slingShotPolyLine("polyLines/slingshot.beam.node")
, _slingShotMaterial("materials/slingshotMaterial.xml")
, _slingShotPosition( Vec2(240,120) )
, _asteroidMesh("meshes/wormholes/wormhole3Fx1_2")
, _asteroidMaterial("materials/wormhole4Fx7.xml")
, _asteroidPosition(Vec2(220,580))
, _asteroidOrientation( -3.1415/2.0)
, _asteroidScale( Vec2(0.5,0.5) )
, _planetMesh("meshes/planet")

, _planetPosition(Vec2(90.0,380.0))
, _planetScale(Vec2(0.25,0.25))
, _bulletSize(25.0)
, _bulletMass(5.0)
, _bulletMoment(100*9*9/4)
, _bulletElasticity(0.5)
, _bulletFriction(0.2)
, _goalSize(40)
, _sideBoardsWidth(1024.0)
, _sideBoardsThickness(32.0)
, _hashCellSize(16)
, _hashCellCount(10000)
{

    _planetMaterial[0] = std::string("materials/planetMaterial.xml");
    _planetMaterial[1] = std::string("materials/planetMaterial.xml");
    _planetMaterial[2] = std::string("materials/planetMaterial.xml");
}

GameConfig::~GameConfig()
{
    
}