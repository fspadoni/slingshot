//
//  Scene1.h
//  Slingshot
//
//  Created by Federico Spadoni on 12/07/16.
//
//

#ifndef __Slingshot__GameScene1__
#define __Slingshot__GameScene1__


#include "cocos2d.h"

#include "physics/CCPhysicsScene.h"


//class GameScene : public cocos2d::Scene
//{
//public:
//    GameScene();
//
//    virtual bool init();
//
//    // implement the "static create()" method manually
//    CREATE_FUNC(GameScene);
//};
namespace cocos2d {
    class PhysicsScene;
    class SoftBodyMaterial;
    class SoftBodyDrawNode;
    
    class RenderPass;
    class SpriteRenderPass;
    
    namespace utils
    {
        class FFMpegVideo;
    }
    
}


class LevelLoader;
class GameLayer1;
class RenderPass;
class SpriteRenderPass;


//namespace CollisionCategory
//{
//    static unsigned int none = 0;
//    static unsigned int actor = 1;
//    static unsigned int slingshotSegments = 2;
//    static unsigned int slingshotNodes = 4;
//    static unsigned int platform = 8;
//    
//    static unsigned int all = ~(unsigned int)0;
//}


class GameScene1 : public cocos2d::PhysicsScene
{
    
    enum {
        _renderTextureTag = 1,
        _gameLayerTag = 2,
        _renderPassTag = 4,
        _finalPassTag = 8
    };
    
public:
    static cocos2d::Scene* create(LevelLoader* loader = nullptr);
    
    virtual void onEnter() override;
    
    virtual void onEnterTransitionDidFinish() override;
    
    virtual void cleanup() override;
    
    //    virtual void update(float delta) override;
    
    virtual void updateThreadSafe(float delta) override;
    
    
    bool loadConfig(const std::string& filename);
    
    bool loadLevels(const std::string& filename);
    
    void setLevel(LevelLoader* levelLoader);
    
    void restartCallback(Ref* sender) { _threadSafeCallbacks.push_back( CC_CALLBACK_0(GameScene1::doRestart, this) ); }
    void mainMenuCallback(Ref* sender) { _threadSafeCallbacks.push_back( CC_CALLBACK_0(GameScene1::doMainMenu, this) ); }
    void toggleDebugCallback(Ref* sender) { _threadSafeCallbacks.push_back( CC_CALLBACK_0(GameScene1::doToggleDebug, this) ); }
    void nextLevel(Ref* sender) { _threadSafeCallbacks.push_back( CC_CALLBACK_0(GameScene1::doNextLevel, this) ); }
    void previousLevel(Ref* sender) { _threadSafeCallbacks.push_back( CC_CALLBACK_0(GameScene1::doPreviousLevel, this) ); }
    
    
protected:
    
    GameScene1(LevelLoader* loader);
    
    virtual ~GameScene1();
    
    virtual bool init() override;
    
    
    bool doToggleDebug();
    bool doRestart();
    bool doMainMenu();
    bool doNextLevel();
    bool doPreviousLevel();
    
    
private:
    
    GameLayer1* _gameLayer;
    
//    cocos2d::RenderPass* _renderPassExtract;
//    cocos2d::RenderPass* _renderPassBlurHorizontal;
//    cocos2d::RenderPass* _renderPassBlurVertical;
//    cocos2d::SpriteRenderPass* _spriteRenderPass;
//    
//    cocos2d::RenderTexture* _renderTarget;
    
    //    LevelLoader* _levelLoader;
    
    bool _debugDraw;
    
    typedef std::function<bool()> ThreadSafeCallback;
    std::vector< ThreadSafeCallback > _threadSafeCallbacks;
    typedef std::vector< ThreadSafeCallback >::const_iterator ThreadSafeCallbacksIter;
    
    
    //    game state ontrol bool vars
    //    bool _toggleDebug;
    //    bool _reStart;
    //    bool _mainMenu;
    //    bool _nextLevel;
    //    bool _previousLevel;
    
    cocos2d::utils::FFMpegVideo* _ffmpegVideo;
    
    FILE* ffmpeg;
    GLubyte* buffer;
    
};


// C++ 11

#define CL(__className__) [](){ return __className__::create();}
#define CLN(__className__) [](){ auto obj = new __className__(); obj->autorelease(); return obj; }



#endif /* defined(__Slingshot__GameScene1__) */
