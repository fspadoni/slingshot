//
//  GameScene.cpp
//  game
//
//  Created by Federico Spadoni on 05/11/14.
//
//

#include "GameScene.h"
#include "Main.h"
#include "GameLayer.h"
#include "LevelLoader.h"
#include "GameConfig.h"

#include "physics-render/CCRenderPass.h"

#include "physics/CCTriangularMesh.h"

#include "physics-render/CCSoftBodyDrawNode.h"

#include "physics-shaders/CCPhysicsShaders.h"


USING_NS_CC;
//USING_NS_CC_EXT;




Scene* GameScene::create(LevelLoader* loader)
{
    
    if (loader == nullptr)
    {
//        const char* levelName = LevelLoader::getFirstLevel() ;
//        loader = LevelLoader::load( levelName );
    }
    
    GameScene *scene = new (std::nothrow) GameScene(loader);
    
    if (scene && scene->init() )
    {
        
        auto layer = GameLayer::create(loader);

        
        layer->setTag(_gameLayerTag);
        
        //    // add layer as a child to scene
        scene->addChild(layer);
        
        scene->autorelease();
        return scene;
    }
    else
    {
        CC_SAFE_DELETE(scene);
        return nullptr;
    }
    
    // return the scene
    return scene;
}


GameScene::GameScene(LevelLoader* loader)
: PhysicsScene( GameConfig::getInstance()->_hashCellSize , GameConfig::getInstance()->_hashCellCount)
//, _levelLoader(loader)
, _debugDraw(false)
, buffer(nullptr)
{
    
}


GameScene::~GameScene()
{
//    _levelLoader->release();
}


bool GameScene::init()
{
    
    if ( !PhysicsScene::initWithPhysics() )
        return false;
    
    
//    _levelLoader->retain();
    
//    _physicsWorld->setAutoStep(false);
    _physicsWorld->setSubsteps(4);
    _physicsWorld->setGravity( GameConfig::getInstance()->_gravity );
    
//    _physicsWorld->setCollisionBias(0.1);
//    _physicsWorld->setCollisionSlop(0.125);
    _physicsWorld->setIterations(12);
//    _physicsWorld->setSleepTimeThreshold(100.0/32.0);
    //    _physicsWorld->setIdleSpeedThreshold();
    
    
    auto visibleSize = Director::getInstance()->getVisibleSize();
    
    _renderPassExtract = cocos2d::RenderPass::create();
//    _renderPassExtract->setOutputTextureSize( visibleSize.width / 2.0, visibleSize.height / 2.0);
    _renderPassExtract->setGLShader( std::string("shaders/bloomExtract.vsh"), std::string("shaders/bloomExtract.fsh") );
    const float bloomThreshold = 0.8;
    _renderPassExtract->getGLProgramState()->setUniformFloatv("bloomThreshold", 1, &bloomThreshold);
    
    
//    const float blur_offset[8] = { 0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0 };
    const float blur_weight[8] = { 0.118, 0.113, 0.100, 0.082, 0.061, 0.042, 0.027, 0.016 };
//    const float blur_offset[4] = { 0.489, 2.451, 4.408, 6.373 };
//    const float blur_weight[4] = { 0.231, 0.182, 0.103, 0.043 };
    
    _renderPassBlurHorizontal = cocos2d::RenderPass::create();
//    _renderPassBlurHorizontal->setOutputTextureSize( visibleSize.width / 2.0, visibleSize.height / 2.0);
    _renderPassBlurHorizontal->setGLShader(std::string("shaders/bloomBlurHorizontal.vsh"), std::string("shaders/bloomBlur.fsh"));
//    _renderPassBlurHorizontal->getGLProgramState()->setUniformFloatv("offset", 4, blur_offset);
    _renderPassBlurHorizontal->getGLProgramState()->setUniformFloatv("weight", 8, blur_weight);
    
    _renderPassBlurVertical = cocos2d::RenderPass::create();
//    _renderPassBlurVertical->setOutputTextureSize( visibleSize.width / 2.0, visibleSize.height / 2.0);
    _renderPassBlurVertical->setGLShader(std::string("shaders/bloomBlurVertical.vsh"), std::string("shaders/bloomBlur.fsh"));
//    _renderPassBlurVertical->getGLProgramState()->setUniformFloatv("offset", 4, blur_offset);
    _renderPassBlurVertical->getGLProgramState()->setUniformFloatv("weight", 8, blur_weight);
    
    _spriteRenderPass = cocos2d::SpriteRenderPass::create(visibleSize.width, visibleSize.height);
    _spriteRenderPass->setGLShader(std::string("shaders/bloomFinalBlend.vsh"), std::string("shaders/bloomFinalBlend.fsh"));
    
    
    return true;
}


void GameScene::onEnter()
{
    Node::onEnter();
    
    _threadSafeCallbacks.clear();
    
    _gameLayer = static_cast<GameLayer*>(getChildByTag(_gameLayerTag) );
    
    
    _renderPassExtract->setInputTexture( _gameLayer->getOutputTexture(), true );
    addChild( (Node*)_renderPassExtract, 32 );
    
    _renderPassBlurHorizontal->setInputTexture( _renderPassExtract->getOutputTexture(), true);
    addChild( (Node*)_renderPassBlurHorizontal, 33 );
    
    _renderPassBlurVertical->setInputTexture( _renderPassBlurHorizontal->getOutputTexture(), true);
    addChild( (Node*)_renderPassBlurVertical, 34 );

    
    _spriteRenderPass->addTexture( _gameLayer->getOutputTexture(), 0 );
    _spriteRenderPass->addTexture( _renderPassBlurVertical->getOutputTexture(), 1 );
    addChild( (Node*)_spriteRenderPass, 36 );
    
    
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    MenuItemFont::setFontSize(24);
    auto startItem = cocos2d::MenuItemFont::create("reStart", CC_CALLBACK_1(GameScene::restartCallback, this));
    auto reStartMenu = Menu::create(startItem, nullptr);
    this->addChild(reStartMenu,64);
    reStartMenu->setPosition(Vec2(origin.x+startItem->getContentSize().width, origin.y+visibleSize.height-startItem->getContentSize().height));
    
    MenuItemFont::setFontSize(24);
    auto stopItem = cocos2d::MenuItemFont::create("stop", CC_CALLBACK_1(GameScene::mainMenuCallback, this));
    auto stopMenu = cocos2d::Menu::create(stopItem, nullptr);
    this->addChild(stopMenu,64);
    stopMenu->setPosition(Vec2(origin.x+visibleSize.width-startItem->getContentSize().width, origin.y+visibleSize.height-startItem->getContentSize().height));
    
    // menu for debug layer
    MenuItemFont::setFontSize(24);
    auto debugItem = cocos2d::MenuItemFont::create("debug", CC_CALLBACK_1(GameScene::toggleDebugCallback, this));
    auto debugMenu = cocos2d::Menu::create(debugItem, nullptr);
    this->addChild(debugMenu,64);
    debugMenu->setPosition(Vec2(origin.x+visibleSize.width-debugItem->getContentSize().width, origin.y+debugItem->getContentSize().height));
    
    

    auto nextLevelItem = MenuItemImage::create("f1.png", "f2.png", CC_CALLBACK_1(GameScene::nextLevel, this) );
    auto prevLevelItem = MenuItemImage::create("b1.png", "b2.png", CC_CALLBACK_1(GameScene::previousLevel, this) );
    
    nextLevelItem->setScale( visibleSize.height / 16 / nextLevelItem->getContentSize().height);
    prevLevelItem->setScale( visibleSize.height / 16 / nextLevelItem->getContentSize().height);
    
    auto levelMenu = cocos2d::Menu::create(nextLevelItem, prevLevelItem, nullptr);
    
    levelMenu->setPosition( origin.x+visibleSize.width/2, origin.y +  nextLevelItem->getContentSize().height);
    nextLevelItem->setPosition( nextLevelItem->getContentSize().width * 1.2 , 0.0 ); //0.0 , VisibleRect::bottom().y+nextLevelItem->getContentSize().height/2);
    prevLevelItem->setPosition( -prevLevelItem->getContentSize().width * 1.2, 0.0 ); //VisibleRect::center().x + prevLevelItem->getContentSize().width*2, VisibleRect::bottom().y+prevLevelItem->getContentSize().height/2);
    
    this->addChild(levelMenu,1);
    
    
    // start ffmpeg telling it to expect raw rgba 720p-60hz frames
    // -i - tells it to read frames from stdin
//    const char* cmd = "/usr/local/Cellar/ffmpeg/2.4.2/bin/ffmpeg -r 30 -f rawvideo -pix_fmt rgba -s 640x900 -i -  -threads 0 -preset fast -y -pix_fmt yuv420p -crf 21 -vf vflip -r 30 output.mp4"; // 640x960
//
////    const char* getcd
////    chdir("/some/where/else");
//    
//    // open pipe to ffmpeg's stdin in binary write mode
//    ffmpeg = popen(cmd, "w");
//    
//    if ( ffmpeg )
//    {
//        const int width = Director::getInstance()->getOpenGLView()->getFrameSize().width;
//        const int height = Director::getInstance()->getOpenGLView()->getFrameSize().height;
//        
//        buffer = new GLubyte[width * height * 4];
//    }

}


void GameScene::onEnterTransitionDidFinish()
{
    Node::onEnterTransitionDidFinish();
    
}

//void GameScene::update(float delta)
//{
//    PhysicsScene::update(delta);
//    
//}


void GameScene::setLevel(LevelLoader* levelLoader)
{
//    _levelLoader = levelLoader;
//    _levelLoader->retain();
}


void GameScene::cleanup()
{
    
    PhysicsScene::cleanup();
    
//    delete buffer;
//    buffer = nullptr;
//    pclose(ffmpeg);
}


bool GameScene::loadConfig(const std::string& filename)
{
    
}


bool GameScene::loadLevels(const std::string& filename)
{
    
}




bool GameScene::doToggleDebug()
{
    _debugDraw = !_debugDraw;
    _physicsWorld->setDebugDrawMask(_debugDraw ? PhysicsWorld::DEBUGDRAW_ALL : PhysicsWorld::DEBUGDRAW_NONE);
    
    _gameLayer->toggleDebugCallback(_debugDraw);
    
    return false;
}


bool GameScene::doRestart()
{
//    Director::getInstance()->pause();
    this->simulationPause();
    
    this->resetPhysicsWorld();
    
    //    auto gameLayer = getChildByTag(_gameLayerTag);
    removeChildByTag(_gameLayerTag);
//    removeChildByTag(_renderPassTag);
//    removeChildByTag(_finalPassTag);
    
    auto layer = GameLayer::create(nullptr);

    
    layer->setTag(_gameLayerTag);
    
    //    // add layer as a child to scene
    this->addChild(layer);
    
    _gameLayer = static_cast<GameLayer*>(getChildByTag(_gameLayerTag) );

    
    _renderPassExtract->setInputTexture( _gameLayer->getOutputTexture() );
    _renderPassBlurHorizontal->setInputTexture( _renderPassExtract->getOutputTexture() );
    _renderPassBlurVertical->setInputTexture( _renderPassBlurHorizontal->getOutputTexture() );
    _spriteRenderPass->addTexture( _gameLayer->getOutputTexture(), 0 );
    _spriteRenderPass->addTexture( _renderPassBlurVertical->getOutputTexture(), 1 );
    
    return true;
}


bool GameScene::doMainMenu()
{
//    Director::getInstance()->pause();
    this->simulationPause();
    
    // create a scene. it's an autorelease object
    auto scene = Main::createScene();
    
    if (scene)
    {
        Director::getInstance()->replaceScene(scene);
    }
    
    return true;
}


bool GameScene::doNextLevel()
{
//    Director::getInstance()->pause();
    this->simulationPause();
    
    this->resetPhysicsWorld();
    
    //    auto gameLayer = getChildByTag(_gameLayerTag);
    removeChildByTag(_gameLayerTag);
    
    
//    const char* nextLevelName = LevelLoader::getNextLevel() ;
//    _levelLoader->release();
//    _levelLoader = LevelLoader::load( nextLevelName );
//    _levelLoader->retain();
    
    auto layer = GameLayer::create(nullptr);
//    callLuaFunction<Layer>("scripts/main.lua","onCreateGameLayer", layer, "cc.Layer");
    
    layer->setTag(_gameLayerTag);
    
    //    // add layer as a child to scene
    this->addChild(layer);
    
    _gameLayer = static_cast<GameLayer*>(getChildByTag(_gameLayerTag) );
    
    return true;
}


bool GameScene::doPreviousLevel()
{
//    Director::getInstance()->pause();
    this->simulationPause();
    
    this->resetPhysicsWorld();
    
    //    auto gameLayer = getChildByTag(_gameLayerTag);
    removeChildByTag(_gameLayerTag);
    
    
//    const char* prevLevelName = LevelLoader::getPreviousLevel() ;
//    _levelLoader->release();
//    _levelLoader = LevelLoader::load( prevLevelName );
//    _levelLoader->retain();
    
    auto layer = GameLayer::create(nullptr);
//    callLuaFunction<Layer>("scripts/main.lua","onCreateGameLayer", layer, "cc.Layer");
    
    layer->setTag(_gameLayerTag);
    
    //    // add layer as a child to scene
    this->addChild(layer);
    
    _gameLayer = static_cast<GameLayer*>(getChildByTag(_gameLayerTag) );
    
    return true;
}


void GameScene::updateThreadSafe(float delta)
{
    ThreadSafeCallbacksIter lastIter = _threadSafeCallbacks.end();
    for (ThreadSafeCallbacksIter iter = _threadSafeCallbacks.begin(); iter != lastIter; ++iter)
    {
        if ( (*iter)() )
        {
            _threadSafeCallbacks.clear();
            return;
        }
    }
    
    _threadSafeCallbacks.clear();

    
    if ( !isSimulationPaused() )
    {
        _gameLayer->updateThreadSafe(delta);
    }
    
    
    // video capture
//    const int width = Director::getInstance()->getOpenGLView()->getFrameSize().width;
//    const int height = Director::getInstance()->getOpenGLView()->getFrameSize().height;
    
    
//    static int captureFrame = 1;
//    
//    if ( ffmpeg && captureFrame )
//    {
//        glPixelStorei(GL_PACK_ALIGNMENT, 1);
//        glReadPixels(0, 0, width, height, GL_RGBA, GL_UNSIGNED_BYTE, buffer);
//        fwrite(buffer, width * height * 4, 1, ffmpeg);
//        captureFrame = 0;
//    }
//    else
//    {
//        captureFrame = 1;
//    }
    
    
}



