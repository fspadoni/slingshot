//
//  Bullet.h
//  Slingshot
//
//  Created by Federico Spadoni on 18/02/16.
//
//

#ifndef __Slingshot__Bullet__
#define __Slingshot__Bullet__

#include "cocos2d.h"
#include "physics/CCPhysicsRigidBody.h"

#include "BulletStatus.h"


NS_CC_BEGIN

class Sprite;

NS_CC_END


class GameLayer;
class SlingShot;



class Bullet : public cocos2d::PhysicsRigidBody
{
public:
    
    //SlingShot status
    //    static StackedStatus Stacked;
	static FiredStatus Fired;
	static ChargedStatus Charged;
//    static ShootingStatus Shooting;
    
    
    static Bullet* create(float radius, float mass, float moment, float elasticity, float friction);
    
    
    static Bullet* clone(const Bullet* other);
    
    
    bool checkForStatus(const cocos2d::PhysicsWorld* physicsWorld);
    
    cocos2d::Sprite* getSprite() const { return _bulletSprite; }
    
    inline bool isInContact() const { return (_contactCounter > 0 ) ? true : false; }
    
    inline void setLayer(GameLayer* layer) { _gameLayer = layer; }
    
    
    virtual void collisionBegin( PhysicsRigidBody* collidingBody, cpArbiter* arb ) override;
    
    virtual void collisionSeparate(PhysicsRigidBody* collidingBody, cpArbiter* arb) override;
    
    
//    bool beginDrawing(cocos2d::PhysicsWorld* physicsWorld, const cocos2d::Vec2& point, const float& maxDistance);
    
//    bool draw(cocos2d::PhysicsWorld* physicsWorld, const cocos2d::Vec2& velocity );
    
//    bool move(cocos2d::PhysicsWorld* physicsWorld, const cocos2d::Vec2& velocity );
//    
//    bool shoot(cocos2d::PhysicsWorld* physicsWorld );
    
    
    void setSlingShot(const SlingShot* slingShot);
    
    BulletStatus* getStatus() { return _currentStatus; }
    
protected:
    
    
    Bullet();
    virtual ~Bullet();
    
    virtual bool init(float radius, float mass, float moment, float elasticity, float friction);
    
    
    
    //    status change
    void changeStatus(BulletStatus* newStatus);
    
	void updateStatus();
    
    
    
private:
    
    cocos2d::Sprite* _bulletSprite;
    
    BulletStatus* _currentStatus;
    
    GameLayer* _gameLayer;
    
    const SlingShot* _slingShot;
    

    float _radius;

    
    int _contactCounter;
    
    
    //    virtual bool init(void);
    
    //    CREATE_FUNC(Block);
    
    friend class FiredStatus;
	friend class ChargedStatus;
    
};


#endif /* defined(__Slingshot__Bullet__) */
