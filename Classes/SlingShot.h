//
//  SlingShot.h
//  Slingshot
//
//  Created by Federico Spadoni on 24/01/16.
//
//

#ifndef __Slingshot__SlingShot__
#define __Slingshot__SlingShot__


#include "cocos2d.h"
#include "physics/CCPhysicsBeamBody.h"

#include "SlingShotStatus.h"

NS_CC_BEGIN
class PolyLine;
class BeamMaterial;
NS_CC_END


//class GameLayer;
class PhysicsRigidBody;


class SlingShot : public cocos2d::PhysicsBeamBody
{
public:
    
    //SlingShot status
//    static StackedStatus Stacked;
	static ActiveStatus Active;
	static DrawingStatus Drawing;
//    static ShootingStatus Shooting;
    
    
    static SlingShot* createFromPolyLine(cocos2d::PolyLine* polyLine, cocos2d::BeamBodyMaterial* material);
    
    static SlingShot* clone(const SlingShot* other);
    

    
    
//    inline void setLayer(GameLayer* layer) { _gameLayer = layer; }
    
    
    bool beginDrawing(cocos2d::PhysicsWorld* physicsWorld, const cocos2d::Vec2& point, const float& maxDistance);
    
    bool draw(cocos2d::PhysicsWorld* physicsWorld, cocos2d::Vec2& velocity );
    
    bool move(cocos2d::PhysicsWorld* physicsWorld, cocos2d::Vec2& velocity );
    
    bool stopMoving(cocos2d::PhysicsWorld* physicsWorld );
    
    bool shoot(cocos2d::PhysicsWorld* physicsWorld );
    
    
    void addBullet(const cocos2d::PhysicsRigidBody* bullet) { _bullets.insert(bullet); }
    
    SlingShotStatus* getStatus() { return _currentStatus; }
    
protected:
    
    
    SlingShot();
    virtual ~SlingShot();
    
    virtual bool init(cocos2d::PolyLine* polyLine, cocos2d::BeamBodyMaterial* material);
    
    
    virtual void sleepBegin(void) override;
    
    virtual void breakElement(const unsigned short segmentIdx) override;
    
    virtual void breakAllElement() override;
    
    virtual void collisionBegin( cpBody* collidingBody, cpArbiter* arb ) override;
    
    virtual void collisionSeparate(cpBody* collidingBody, cpArbiter* arb) override;
    
    
    //    status change
    void changeStatus(SlingShotStatus* newStatus);
    
	void updateStatus();
    
    
    
private:
    
    
//    GameLayer* _gameLayer;
    
    SlingShotStatus* _currentStatus;
    
    std::set<const cocos2d::PhysicsRigidBody*> _bullets;
    
    unsigned short endPointIndices[2];
    
    unsigned short _drawingNodeId;
    
    
    
    //    virtual bool init(void);
    
    //    CREATE_FUNC(Block);
    
    
};




#endif /* defined(__Slingshot__SlingShot__) */
