//
//  LevelScene.hpp
//  Slingshot
//
//  Created by Federico Spadoni on 23/10/16.
//
//

#ifndef LevelScene_hpp
#define LevelScene_hpp

#include "physics/CCPhysicsScene.h"
#include "2d/CCLayer.h"


namespace cocos2d {
    class SoftBodyMaterial;
    class SoftBodyDrawNode;
    
    namespace utils
    {
        class FFMpegVideo;
    }
    
}

class LevelLoader;
class LevelLayer;



class LevelScene : public cocos2d::PhysicsScene
{
    
    enum {
        _renderTextureTag = 1,
        _gameLayerTag = 2,
        _renderPassTag = 4,
        _finalPassTag = 8
    };
    
public:
    
    static LevelScene* create(LevelLoader* loader = nullptr);
    
    
    virtual void onEnter() override;
    
    virtual void onEnterTransitionDidFinish() override;
    
    virtual void cleanup() override;
    
    //    virtual void update(float delta) override;
    
    virtual void updateThreadSafe(float delta) override;
    
    
    bool loadConfig(const std::string& filename);
    
    bool loadLevels(const std::string& filename);
    
    void setLevel(LevelLoader* levelLoader);
    
    void restartCallback(Ref* sender) { _threadSafeCallbacks.push_back( CC_CALLBACK_0(LevelScene::doRestart, this) ); }
    void mainMenuCallback(Ref* sender) { _threadSafeCallbacks.push_back( CC_CALLBACK_0(LevelScene::doMainMenu, this) ); }
    void toggleDebugCallback(Ref* sender) { _threadSafeCallbacks.push_back( CC_CALLBACK_0(LevelScene::doToggleDebug, this) ); }
    void nextLevel(Ref* sender) { _threadSafeCallbacks.push_back( CC_CALLBACK_0(LevelScene::doNextLevel, this) ); }
    void previousLevel(Ref* sender) { _threadSafeCallbacks.push_back( CC_CALLBACK_0(LevelScene::doPreviousLevel, this) ); }
    
    
protected:
    
    LevelScene(LevelLoader* loader);
    
    virtual ~LevelScene();
    
    virtual bool init() override;
    
    
    bool doToggleDebug();
    bool doRestart();
    bool doMainMenu();
    bool doNextLevel();
    bool doPreviousLevel();
    
    
private:
    
    LevelLayer* _layer;
    
    
    bool _debugDraw;
    
    typedef std::function<bool()> ThreadSafeCallback;
    std::vector< ThreadSafeCallback > _threadSafeCallbacks;
    typedef std::vector< ThreadSafeCallback >::const_iterator ThreadSafeCallbacksIter;
    
    
    cocos2d::utils::FFMpegVideo* _ffmpegVideo;
    
    FILE* ffmpeg;
    GLubyte* buffer;
    
};


// C++ 11

#define CL(__className__) [](){ return __className__::create();}
#define CLN(__className__) [](){ auto obj = new __className__(); obj->autorelease(); return obj; }





class LevelLayer : public cocos2d::Layer
{
public:
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();
    
    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();
    
    virtual void onEnter() override;
    virtual void onExit() override;
    virtual void cleanup() override;
    
    
    //    void postScriptInit(cocos2d::Layer* layer);
    
    void menuPlayCallback(cocos2d::Ref* pSender);
    
    // a selector callback
    void menuCloseCallback(cocos2d::Ref* pSender);
    
    // implement the "static create()" method manually
    CREATE_FUNC(LevelLayer);
    
private:
    
    
    
};

#endif /* LevelScene_hpp */
