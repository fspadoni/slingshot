//
//  BulletStatus.h
//  Slingshot
//
//  Created by Federico Spadoni on 18/02/16.
//
//

#ifndef __Slingshot__BulletStatus__
#define __Slingshot__BulletStatus__

class Bullet;


class BulletStatus
{
public:
    
	virtual ~BulletStatus() {}
    
    
	virtual void enter(Bullet*) = 0;
    
	virtual void execute(Bullet*) = 0;
    
	virtual void exit(Bullet*) = 0;
};


class FiredStatus : public BulletStatus
{
    
public:
    
    virtual ~FiredStatus() {}
    
	virtual void enter(Bullet*)
	{
	}
    
	virtual void execute(Bullet*)
	{
	}
    
	virtual void exit(Bullet*)
	{
	}
    
};

class ChargedStatus : public BulletStatus
{
    
public:
    
    virtual ~ChargedStatus() {}
    
	virtual void enter(Bullet*)
	{
	}
    
	virtual void execute(Bullet*)
	{
	}
    
	virtual void exit(Bullet*)
	{
	}
    
};


#endif /* defined(__Slingshot__BulletStatus__) */
