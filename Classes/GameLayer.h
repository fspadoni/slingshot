//
//  GameLayer.h
//  game
//
//  Created by Federico Spadoni on 10/04/15.
//
//

#ifndef __game__GameLayer__
#define __game__GameLayer__


#include "cocos2d.h"

#include "physics-render/CCRenderTextureLayer.h"

#include "physics-render/CCBeamBodyDrawNodeT.h"

namespace cocos2d {
    class PhysicsScene;
    class BeamBodyMaterial;
    class BeamBodyDrawNode;
    class SoftBodyMaterial;
    class SoftBodyDrawNode;
    class PhysicsSpriteNode;

}


class LevelLoader;
class SlingShot;
class Bullet;
class Asteroid;




class GameLayer : public cocos2d::RenderTextureLayer //public cocos2d::Layer
{
    
    
    GameLayer(const LevelLoader* loader);
    
    virtual ~GameLayer();
    
public:
    
    static GameLayer* create(const LevelLoader* loader);
    
    
    virtual bool init();
    
    virtual void onEnter() override;
    virtual void onExit() override;
    virtual void cleanup() override;
    
    virtual void update(float delta) override;
    
    void updateThreadSafe(float delta);
    
    
    void toggleDebugCallback(bool debugDraw);
    
    void setScene(cocos2d::PhysicsScene* scene) { _scene = scene; }
    
    //    typedef std::function<bool(PhysicsWorld&, PhysicsShape&, void*)> PhysicsQueryRectCallbackFunc;
    //    cocos2d::PhysicsQueryRectCallbackFunc levelCompleted;
    bool checkForLevelCompletion(cocos2d::PhysicsWorld& world, cocos2d::PhysicsShape& shape, void* data);
    bool levelCompleted(cocos2d::PhysicsWorld& world, cocos2d::PhysicsShape& shape, void* data);
    
    void increaseBlockFragmentCounter();
    void increaseBlockBrokenCounter();
    
private:
    
    void initBoard();
    
    bool initLevel();
    
    
    void addBeamBodyDrawNode(cocos2d::BeamBodyDrawNodeT<cocos2d::V2F_N2F_T2F_C4F>* node, cocos2d::BeamBodyMaterial* mat)
        {_beambodyDrawNodesT[mat] = node;}
    
    void addSoftBodyDrawNode(cocos2d::SoftBodyDrawNode* node, cocos2d::SoftBodyMaterial* mat)
    {_softbodyDrawNodes[mat] = node;}
    
    void pushNextBlock(float dt);
    
    
    //    void pushNextBlock(cocos2d::PhysicsSoftBody* block);
    
    virtual bool onTouchBegan(cocos2d::Touch* touch, cocos2d::Event* event);
    virtual void onTouchMoved(cocos2d::Touch* touch, cocos2d::Event* event);
    virtual void onTouchEnded(cocos2d::Touch* touch, cocos2d::Event* event);
    
    void onTouchMovedOld(cocos2d::Touch* touch, cocos2d::Event* event);
    void onTouchMovedOldOld(cocos2d::Touch* touch, cocos2d::Event* event);
    
    virtual void onKeyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* event);
    virtual void onKeyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* event);
    
    
//    threadSafe callbacks
    bool doTouchBegan();
    bool doTouchMoved();
    bool doTouchEnded();
    
    bool doPushNextBlock();
    bool doLevelCompleted();
    bool doIncreaseBlockFragmentCounter();
    bool doIncreaseBrokenCounter();
    bool doStackFailed();
    
    
    typedef std::function<bool()> ThreadSafeCallback;
    typedef std::vector< ThreadSafeCallback >::const_iterator ThreadSafeCallbacksIter;
    std::vector< ThreadSafeCallback > _threadSafeCallbacks;
    
    
//    const LevelLoader* _loader;
    
    cocos2d::PhysicsScene* _scene;
    cocos2d::PhysicsWorld* _physicsWorld;
    //    bool _debugDraw;
    

    cocos2d::PhysicsSpriteNode* physicsSpriteNode;
    
    SlingShot* _slingShot;
    
    typedef std::map<const cocos2d::BeamBodyMaterial* ,cocos2d::BeamBodyDrawNode*> BeamBodyDrawNodes;
    typedef BeamBodyDrawNodes::iterator BeamBodyDrawNodesIterator;
    typedef BeamBodyDrawNodes::const_iterator BeamBodyDrawNodesConstIterator;

    BeamBodyDrawNodes _beambodyDrawNodes;
    
    
    typedef std::map<const cocos2d::BeamBodyMaterial* ,cocos2d::BeamBodyDrawNodeT<cocos2d::V2F_N2F_T2F_C4F>*> BeamBodyDrawNodesT;
    typedef BeamBodyDrawNodesT::iterator BeamBodyDrawNodesTiterator;
    typedef BeamBodyDrawNodesT::const_iterator BeamBodyDrawNodesTConstiterator;
    
    BeamBodyDrawNodesT _beambodyDrawNodesT;
    
    
    
    cocos2d::Vector<cocos2d::PhysicsSoftBody*> _asteroids;
    
    typedef std::map<const cocos2d::SoftBodyMaterial* ,cocos2d::SoftBodyDrawNode*> SoftBodyDrawNodes;
    typedef std::map<const cocos2d::SoftBodyMaterial* ,cocos2d::SoftBodyDrawNode*>::iterator SoftBodyDrawNodesIterator;
    typedef std::map<const cocos2d::SoftBodyMaterial* ,cocos2d::SoftBodyDrawNode*>::const_iterator SoftBodyDrawNodesConstIterator;
    
    SoftBodyDrawNodes _softbodyDrawNodes;

    

    Bullet* _bullet;
    
    cocos2d::Vector<cocos2d::PhysicsRigidBody*> _goals;
    
    cocos2d::PhysicsSoftBody* _planet;
    
    cocos2d::PhysicsRigidBody* _bottomBody;
    
    cocos2d::Vector<cocos2d::PhysicsRigidBody*> _staticBoards;
    
//    cocos2d::Vector<Block*> _levelLoadedBlocks;
    

    
    cocos2d::Label* _scoreLabel;
    cocos2d::Sprite* _scoreIcon;

    bool _isTouching;
    
    cocos2d::Vec2 _delta;
    
    
};




#endif /* defined(__game__GameLayer__) */
