//
//  GameScene.h
//  game
//
//  Created by Federico Spadoni on 05/11/14.
//
//

#ifndef game_GameScene_h
#define game_GameScene_h

#include "cocos2d.h"

#include "physics/CCPhysicsScene.h"


//class GameScene : public cocos2d::Scene
//{
//public:
//    GameScene();
//    
//    virtual bool init();
//    
//    // implement the "static create()" method manually
//    CREATE_FUNC(GameScene);
//};
namespace cocos2d {
    class PhysicsScene;
    class SoftBodyMaterial;
    class SoftBodyDrawNode;
    
    class RenderPass;
    class SpriteRenderPass;
    
    namespace utils
    {
        class FFMpegVideo;
    }

}


class LevelLoader;
class GameLayer;
class RenderPass;
class SpriteRenderPass;




class GameScene : public cocos2d::PhysicsScene
{
    
    enum {
            _renderTextureTag = 1,
            _gameLayerTag = 2,
            _renderPassTag = 4,
            _finalPassTag = 8
    };
    
public:
    static cocos2d::Scene* create(LevelLoader* loader = nullptr);
    
    virtual void onEnter() override;
    
    virtual void onEnterTransitionDidFinish() override;
    
    virtual void cleanup() override;
    
//    virtual void update(float delta) override;
    
    virtual void updateThreadSafe(float delta) override;
    
    
    bool loadConfig(const std::string& filename);
    
    bool loadLevels(const std::string& filename);
    
    void setLevel(LevelLoader* levelLoader);
    
    void restartCallback(Ref* sender) { _threadSafeCallbacks.push_back( CC_CALLBACK_0(GameScene::doRestart, this) ); }
    void mainMenuCallback(Ref* sender) { _threadSafeCallbacks.push_back( CC_CALLBACK_0(GameScene::doMainMenu, this) ); }
    void toggleDebugCallback(Ref* sender) { _threadSafeCallbacks.push_back( CC_CALLBACK_0(GameScene::doToggleDebug, this) ); }
    void nextLevel(Ref* sender) { _threadSafeCallbacks.push_back( CC_CALLBACK_0(GameScene::doNextLevel, this) ); }
    void previousLevel(Ref* sender) { _threadSafeCallbacks.push_back( CC_CALLBACK_0(GameScene::doPreviousLevel, this) ); }

    
protected:
    
    GameScene(LevelLoader* loader);
    
    virtual ~GameScene();
    
    virtual bool init() override;
    
    
    bool doToggleDebug();
    bool doRestart();
    bool doMainMenu();
    bool doNextLevel();
    bool doPreviousLevel();
    
    
private:
    
    GameLayer* _gameLayer;
    
    cocos2d::RenderPass* _renderPassExtract;
    cocos2d::RenderPass* _renderPassBlurHorizontal;
    cocos2d::RenderPass* _renderPassBlurVertical;
    cocos2d::SpriteRenderPass* _spriteRenderPass;
    
    cocos2d::RenderTexture* _renderTarget;
    
//    LevelLoader* _levelLoader;
    
    bool _debugDraw;
    
    typedef std::function<bool()> ThreadSafeCallback;
    std::vector< ThreadSafeCallback > _threadSafeCallbacks;
    typedef std::vector< ThreadSafeCallback >::const_iterator ThreadSafeCallbacksIter;
    
    
//    game state ontrol bool vars
//    bool _toggleDebug;
//    bool _reStart;
//    bool _mainMenu;
//    bool _nextLevel;
//    bool _previousLevel;
    
    cocos2d::utils::FFMpegVideo* _ffmpegVideo;
    
    FILE* ffmpeg;
    GLubyte* buffer;
    
};


// C++ 11

#define CL(__className__) [](){ return __className__::create();}
#define CLN(__className__) [](){ auto obj = new __className__(); obj->autorelease(); return obj; }


#endif
