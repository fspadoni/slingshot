//
//  GameLayer.cpp
//  game
//
//  Created by Federico Spadoni on 10/04/15.
//
//

#include "GameLayer.h"
#include "GameScene.h"

#include "GameConfig.h"
#include "LevelLoader.h"

#include "physics-render/CCBeamBodyDrawNode.h"
#include "physics-render/CCSoftBodyDrawNode.h"
#include "physics/CCPolyLine.h"
#include "physics/CCTriangularMesh.h"
#include "physics/CCMaterialProperty.h"
#include "physics-render/CCPhysicsSpriteNode.h"
#include "physics/CCPhysicsRigidShape.h"
#include "physics/CCPhysicsRigidBody.h"

#include "physics-render/CCScrollingSprite.h"
#include "physics-actions/CCPhysicsActionInterval.h"

#include "SlingShot.h"
#include "Bullet.h"
#include "Asteroid.h"


USING_NS_CC;

const float PI_2 = 2.0 * 3.1415926535897932385f;



GameLayer* GameLayer::create(const LevelLoader* loader)
{
    GameLayer *ret = new (std::nothrow) GameLayer(loader);
    if (ret && ret->init())
    {
        ret->autorelease();
        return ret;
    }
    else
    {
        CC_SAFE_DELETE(ret);
        return nullptr;
    }
}



GameLayer::GameLayer(const LevelLoader* loader)
: RenderTextureLayer()
//_loader(loader)
, _scene(nullptr)
, _physicsWorld(nullptr)
, _slingShot(nullptr)
, _scoreLabel(nullptr)
, _scoreIcon(nullptr)
, _isTouching(false)
{
    
}

GameLayer::~GameLayer()
{
    _bullet->release();
    _bottomBody->release();
//    _goal->release();
}


bool GameLayer::init()
{
    RenderTextureLayer::init();
    
    Node::scheduleUpdate();
    
    
    physicsSpriteNode = PhysicsSpriteNode::create();
    this->addChild(physicsSpriteNode, 2);

    
    initBoard();
    
    bool initialized = initLevel();
    
    return initialized;
}


void GameLayer::initBoard()
{

    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
//    float scaleFactor = Director::getInstance()->getContentScaleFactor();
    
    const float width = GameConfig::getInstance()->_sideBoardsWidth;
	const float thickness = GameConfig::getInstance()->_sideBoardsThickness;
    
    
    auto background = cocos2d::Sprite::create("textures/spaceBackground.png");
    background->setScale( visibleSize.width/background->getContentSize().width*1.5,
                         visibleSize.height/background->getContentSize().height*1.1 );
    background->setPosition(origin+(visibleSize/2.0));
    this->addChild(background, -2);
    
    _bottomBody = cocos2d::PhysicsRigidBody::create(false);
    _bottomBody->retain();
    _bottomBody->addShape(PhysicsRigidShape::createBox( Size(width, thickness),  0.0, 0.5));
    _bottomBody->setCollisionFilter( CollisionCategory::platform, ~CollisionCategory::platform );

    _bottomBody->setPosition( Vec2(origin.x + visibleSize.width / 2, -thickness ) );
    
    

    auto staticBoard = cocos2d::PhysicsRigidBody::create(false);
    staticBoard->addShape(PhysicsRigidShape::createBox( Size(thickness, width),  1.0, 0.5));
    staticBoard->setCollisionFilter( CollisionCategory::platform, ~CollisionCategory::platform );
    staticBoard->setPosition( Vec2(origin.x,  origin.y + visibleSize.height / 2.0) );
    
    auto leftBoard = Sprite::create("textures/boardLeftRight.png");
    leftBoard->setScale( visibleSize.height/leftBoard->getContentSize().height *1.1);
    physicsSpriteNode->addChildAndRigidBody(leftBoard, staticBoard, 0);
    
    _staticBoards.pushBack(staticBoard);
    
    
    staticBoard = cocos2d::PhysicsRigidBody::create(false);
    staticBoard->addShape(PhysicsRigidShape::createBox( Size(thickness, width),  1.0, 0.5));
    staticBoard->setCollisionFilter( CollisionCategory::platform, ~CollisionCategory::platform );
    staticBoard->setPosition( Vec2(origin.x + visibleSize.width,  origin.y + visibleSize.height / 2.0) );

    
    auto rightBoard = cocos2d::Sprite::create("textures/boardLeftRight.png");
    rightBoard->setScale( visibleSize.height/rightBoard->getContentSize().height *1.1);
    physicsSpriteNode->addChildAndRigidBody(rightBoard, staticBoard, 0);
    
    _staticBoards.pushBack(staticBoard);
    
    
    staticBoard = cocos2d::PhysicsRigidBody::create(false);
    staticBoard->addShape(PhysicsRigidShape::createBox( Size(width, thickness),  0.5, 0.5));
    staticBoard->setCollisionFilter( CollisionCategory::platform, ~CollisionCategory::platform );
    staticBoard->setPosition( Vec2(origin.x + visibleSize.width / 2, visibleSize.height + thickness) );

    _staticBoards.pushBack(staticBoard);
    
    
}




bool GameLayer::initLevel()
{
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    //    bullet
    _bullet = Bullet::create( GameConfig::getInstance()->_bulletSize *0.5,
                              GameConfig::getInstance()->_bulletMass,
                              GameConfig::getInstance()->_bulletMoment,
                              GameConfig::getInstance()->_bulletElasticity,
                              GameConfig::getInstance()->_bulletFriction
                             );
    _bullet->retain();
    _bullet->setCollisionFilter( CollisionCategory::actor,
                                CollisionCategory::slingshotSegments | CollisionCategory::actor | CollisionCategory::platform);
    _bullet->setPosition( GameConfig::getInstance()->_slingShotPosition + Vec2(50,64) );
    //create a sprite
//    auto bulletSprite = Sprite::create("textures/bulletFireball.png");
//    float scaleX = GameConfig::getInstance()->_bulletSize / bulletSprite->getContentSize().width;
//    float scaleY = GameConfig::getInstance()->_bulletSize / bulletSprite->getContentSize().height;
//    bulletSprite->setScale(scaleX, scaleY );
    
    physicsSpriteNode->addChildAndRigidBody(_bullet->getSprite(), _bullet);
    
    
    
    std::string slingShotFullpath = FileUtils::getInstance()->fullPathForFilename(GameConfig::getInstance()->_slingShotPolyLine);
    auto polyLine = PolyLine::createFromFile(slingShotFullpath);
    
    std::string materialFullpath = FileUtils::getInstance()->fullPathForFilename(GameConfig::getInstance()->_slingShotMaterial);
    if ( materialFullpath.size() == 0)
    {
        return false;
    }
    auto material = BeamBodyMaterial::createFromFile(materialFullpath);
    
    _slingShot = SlingShot::createFromPolyLine( polyLine, material);
    _slingShot->setPosition(GameConfig::getInstance()->_slingShotPosition);
//    _slingShot->setCollisionFilter(CollisionCategory::actor);
    _slingShot->setCollisionFilter(CollisionCategory::slingshotSegments | CollisionCategory::slingshotNodes );
 
    
    _slingShot->addBullet(_bullet);
    
//    DrawNode

    auto drawNode = BeamBodyDrawNodeT<cocos2d::V2F_N2F_T2F_C4F>::create( material->getTextureFileName() );
    addBeamBodyDrawNode( drawNode, material );
    drawNode->setWireFrame(false);
    drawNode->setThickness(6.0); //32.0
    this->addChild(drawNode);
    
//    auto glShaderProgram = GLProgram::createWithByteArrays(ccShader_2D_LINE_POW_vert, ccShader_2D_LINE_POW_frag  );
//    auto glShaderProgram = GLProgram::createWithFilenames("shaders/slingShot.vsh", "shaders/slingShot.fsh"  );
//    auto glProgramState = GLProgramState::getOrCreateWithGLProgram(glShaderProgram);
//    drawNode->setGLProgramState(glProgramState);
    

    
    
//    std::string asteroidFullpath = FileUtils::getInstance()->fullPathForFilename(GameConfig::getInstance()->_asteroidMesh);
    std::string asteroidFullpath = GameConfig::getInstance()->_asteroidMesh;
    
//    auto mesh = TriangularMesh::createFromFile(asteroidFullpath);
//    mesh->setScale(GameConfig::getInstance()->_scaleAsteroid.x);
    
    materialFullpath = FileUtils::getInstance()->fullPathForFilename(GameConfig::getInstance()->_asteroidMaterial);
    
//    if ( materialFullpath.size() == 0)
//        return false;
    
//    auto asteroidMaterial = SoftBodyMaterial::createFromFile(materialFullpath);
    
//    _asteroid = PhysicsSoftBody::createFromTriangularMesh(mesh, asteroidMaterial);
    
//    _asteroid->setCollisionFilter(CollisionCategory::actor);
    
    auto asteroid = Asteroid::create(asteroidFullpath, materialFullpath, GameConfig::getInstance()->_asteroidScale.x);
    
    asteroid->translate(GameConfig::getInstance()->_asteroidPosition);
    
    auto asteroidDrawNode = cocos2d::SoftBodyDrawNode::create( asteroid->getMaterials()->getTextureFileName() );
    addSoftBodyDrawNode( asteroidDrawNode, asteroid->getMaterials() );
    
    this->addChild(asteroidDrawNode, 0);
    
    _asteroids.pushBack(asteroid);
    
    
    auto mesh = asteroid->getTriangularMesh();
    
    if ( mesh->getNumMarkedPoints() > 0 )
    {
        for (int i=0; i<mesh->getNumMarkedPoints(); i+=2)
        {
            const Vec2* markedPoints = mesh->getMarkedPoints();
            
            cocos2d::PhysicsRigidBody* goal = cocos2d::PhysicsRigidBody::create(GameConfig::getInstance()->_bulletMass,
                                                      GameConfig::getInstance()->_bulletMoment*100);
            goal->addShape(PhysicsRigidShape::createCircle(8.0, 0.0, 0.5));
            
            goal->setCollisionFilter( CollisionCategory::actor,
                                      CollisionCategory::slingshotSegments | CollisionCategory::actor | CollisionCategory::platform);
            goal->setPosition( markedPoints[i] +  GameConfig::getInstance()->_asteroidPosition );
            
            _goals.pushBack(goal);
            
            //create a sprite
            auto goalSprite = cocos2d::Sprite::create("textures/gems1.png");
            float scaleX = GameConfig::getInstance()->_goalSize / goalSprite->getContentSize().width;
            float scaleY = GameConfig::getInstance()->_goalSize / goalSprite->getContentSize().height;
            goalSprite->setScale(scaleX, scaleY );
            
            physicsSpriteNode->addChildAndRigidBody(goalSprite, goal);
            
        }
    }
    
    
    std::string planetFullpath = GameConfig::getInstance()->_planetMesh;
    auto planetMesh = TriangularMesh::createFromFile(GameConfig::getInstance()->_planetMesh);
    planetMesh->setScale(GameConfig::getInstance()->_planetScale.x);
    
    materialFullpath = FileUtils::getInstance()->fullPathForFilename(GameConfig::getInstance()->_planetMaterial[0]);
    auto planetMaterial = SoftBodyMaterial::createFromFile(materialFullpath);

    _planet = PhysicsSoftBody::createFromTriangularMesh(planetMesh, planetMaterial);
    
    _planet->setCollisionFilter( CollisionCategory::actor, CollisionCategory::actor);

    _planet->translate(GameConfig::getInstance()->_planetPosition);
    
    auto planetDrawNode = cocos2d::SoftBodyDrawNode::create( _planet->getMaterials()->getTextureFileName() );
    addSoftBodyDrawNode( planetDrawNode, _planet->getMaterials() );
    
    this->addChild(planetDrawNode, 0);
    
    _asteroids.pushBack(_planet);

    
    
    
//    const Vector<LevelBlock*>& levelBlocks = _loader->getLevelBlocks();
    
//    Vector<LevelBlock*>::const_iterator lastIter = levelBlocks.cend();
    
//    for (Vector<LevelBlock*>::const_iterator iter = levelBlocks.cbegin(); iter != lastIter; ++iter )
//    {
//        
//        const TriangularMesh* mesh = (*iter)->getMesh();
//        const SoftBodyMaterial* material = (*iter)->getMaterial();
//        
//        const Vec2 pos = (*iter)->getPosition();
//    }
    
    
    return true;
}

void GameLayer::onEnter()
{
    Layer::onEnter();
    
    _scene = dynamic_cast<cocos2d::PhysicsScene*>(this->getParent());
    
    //    add blocks loaded from level to the scene
    
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();


    _bullet->setSlingShot(_slingShot);
    _scene->addRigidBodyToPhysicsWorld(_bullet);
    _scene->addBeamBodyToPhysicsWorld(_slingShot);
    _scene->addRigidBodyToPhysicsWorld(_bottomBody);
    for (const auto& goal : _goals )
    {
        _scene->addRigidBodyToPhysicsWorld(goal);
    }
    for (const auto& board : _staticBoards)
    {
        _scene->addRigidBodyToPhysicsWorld(board);
    }
    
    
    const BeamBodyMaterial* material = _slingShot->getMaterial();
    BeamBodyDrawNodesTiterator drawNodeIter = _beambodyDrawNodesT.find(material);
    BeamBodyDrawNodeT<cocos2d::V2F_N2F_T2F_C4F>* drawNode = drawNodeIter->second;
    drawNode->addBeamBody(_slingShot);
//    ?
//    _slingShot->setLayer(this);
    
    
    for (auto asteroid : _asteroids)
    {
        _scene->addSoftBodyToPhysicsWorld(asteroid);
        
        SoftBodyDrawNodesIterator softbodyDrawNodeIter = _softbodyDrawNodes.find( asteroid->getMaterials() );
        cocos2d::SoftBodyDrawNode* softbodyDrawNode = softbodyDrawNodeIter->second;
        softbodyDrawNode->addSoftBody(asteroid);
    }
    
    

    
    auto moveBy = cocos2d::PhysicsMoveBy::create(4.0, Vec2(_planet->getPosition().x + 2*(visibleSize.width/2-_planet->getPosition().x) , 0.0) / ( 30.0) );
    auto moveByBack = moveBy->reverse();
    auto delay = cocos2d::DelayTime::create(0.5f);
    auto seq = cocos2d::PhysicsSequence::create(moveBy, delay, moveByBack, delay, nullptr);
    _planet->runAction(PhysicsRepeatForever::create(seq));
    
    
    auto rotateBy = cocos2d::PhysicsRotateBy::create(20.0f, 360.0f);
    (*_asteroids.begin())->runAction(PhysicsRepeatForever::create(rotateBy));
    
    
// Register Touch Event
//    setTouchEnabled(true);
//    setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    
    listener->onTouchBegan = CC_CALLBACK_2(GameLayer::onTouchBegan, this);
    listener->onTouchMoved = CC_CALLBACK_2(GameLayer::onTouchMoved, this);
    listener->onTouchEnded = CC_CALLBACK_2(GameLayer::onTouchEnded, this);
    
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
    _touchListener = listener;
    
    // Register Keyboard Event
    //    setKeyboardEnabled(true);
    _keyboardListener = EventListenerKeyboard::create();
    _keyboardListener->onKeyPressed = CC_CALLBACK_2(GameLayer::onKeyPressed, this);
    _keyboardListener->onKeyReleased = CC_CALLBACK_2(GameLayer::onKeyReleased, this);
    
    _eventDispatcher->addEventListenerWithSceneGraphPriority(_keyboardListener, this);
    
    
    
    
//    Director::getInstance()->Director::resume();
//    this->resume();
    _scene->simulationResume();
    
    _physicsWorld = _scene->getPhysicsWorld();
    
}

void GameLayer::onExit()
{
    
    Layer::onExit();
}

void GameLayer::cleanup()
{
    
    Layer::cleanup();
}


void GameLayer::update(float delta)
{
    
    Node::update(delta);
    
}

void GameLayer::updateThreadSafe(float delta)
{
    
    static const float frequency = 1.0 / Director::getInstance()->getAnimationInterval();
    
//    check for bullet status:
    _bullet->checkForStatus( _physicsWorld );
    
    if (_isTouching )
    {
        doTouchMoved();
    }
    
    ThreadSafeCallbacksIter lastIter = _threadSafeCallbacks.end();
    for (ThreadSafeCallbacksIter iter = _threadSafeCallbacks.begin(); iter != lastIter; ++iter)
    {
        if ( (*iter)() )
        {
            _threadSafeCallbacks.clear();
            return;
        }
    }
    
    _threadSafeCallbacks.clear();

}


void GameLayer::pushNextBlock(float dt)
{
    _threadSafeCallbacks.push_back( CC_CALLBACK_0(GameLayer::doPushNextBlock, this) );
}

void GameLayer::increaseBlockFragmentCounter()
{
//    ++_blockFragmentCounter;
    _threadSafeCallbacks.push_back( CC_CALLBACK_0(GameLayer::doIncreaseBlockFragmentCounter, this) );
}

void GameLayer::increaseBlockBrokenCounter()
{
    
    _threadSafeCallbacks.push_back( CC_CALLBACK_0(GameLayer::doIncreaseBrokenCounter, this) );
   
//    if ( --_blockBrokenCounter == 0 )
    {
        _threadSafeCallbacks.push_back( CC_CALLBACK_0(GameLayer::doStackFailed, this) );
    }
}

bool GameLayer::doIncreaseBlockFragmentCounter()
{
    char buffer[8];
//    sprintf(buffer, "%d", _blockFragmentCounter );
//    _scoreLabel->setString(buffer);
    
    return false;
}

bool GameLayer::doIncreaseBrokenCounter()
{
    
    return false;
}

bool GameLayer::doStackFailed()
{
    auto failLevel = Label::createWithTTF("Stack Failed", "fonts/Marker Felt.ttf", 48);
//    failLevel->setPosition(VisibleRect::center());
    this->addChild(failLevel);
    
    _scene->simulationPause();
//    this->pause();
    
    return true;
}

bool GameLayer::doPushNextBlock()
{
    
    
    return false;
}

bool GameLayer::doTouchBegan()
{

    _isTouching = true;
    
    if ( _slingShot->beginDrawing(_physicsWorld, _delta, 100) )
    {

    }
    
    _delta = Vec2::ZERO;
    
    return false;
}


bool GameLayer::doTouchMoved()
{
    static const float frequency = 1.0 / Director::getInstance()->getAnimationInterval();
    
//    CCLOG("delta %f %f",_delta.x, _delta.y);
    
    if ( _bullet->getStatus() == &Bullet::Charged )
    {
        Vec2 velocity = _delta * frequency;
        _slingShot->draw( _physicsWorld, velocity );
    
    }
    else if ( _bullet->getStatus() == &Bullet::Fired )
    {
        Vec2 velocity = _delta * frequency;
        _slingShot->move(_physicsWorld, velocity);
        
    }
    
    _delta = Vec2::ZERO;
    
    return false;
}



bool GameLayer::doTouchEnded()
{

    if ( _bullet->getStatus() == &Bullet::Charged )
    {
        _slingShot->shoot(_physicsWorld);
    }
    
    _slingShot->stopMoving(_physicsWorld);
    
    _isTouching = false;
    
    return false;
}


bool GameLayer::onTouchBegan(Touch* touch, Event* event)
{
    
    _delta = touch->getLocation();
//    _startLocation = touch->getLocation();
    
    _threadSafeCallbacks.push_back( CC_CALLBACK_0(GameLayer::doTouchBegan, this) );
    
    return true;
}


void GameLayer::onTouchMoved(Touch* touch, Event* event)
{

    _delta = touch->getDelta();

//    _threadSafeCallbacks.push_back( CC_CALLBACK_0(GameLayer::doTouchMoved, this) );
}



void GameLayer::onTouchEnded(Touch* touch, Event* event)
{
    _threadSafeCallbacks.push_back( CC_CALLBACK_0(GameLayer::doTouchEnded, this) );
}


void GameLayer::onKeyPressed(EventKeyboard::KeyCode keyCode, Event* unused_event)
{
    static const float dx = 4;
    static const float da = 2.0 * PI_2 / 120.0;
    
//    if ( keyCode == EventKeyboard::KeyCode::KEY_LEFT_ARROW )
//    {
//        _keyTranslate = true;
//        _delta.x = -dx;
//    }
//    if (keyCode == EventKeyboard::KeyCode::KEY_RIGHT_ARROW )
//    {
//        _keyTranslate = true;
//        _delta.x = dx;
//    }
//    if ( keyCode == EventKeyboard::KeyCode::KEY_UP_ARROW )
//    {
//        _keyRotate = true;
//        _delta.y = da;
//    }
//    if (keyCode == EventKeyboard::KeyCode::KEY_DOWN_ARROW )
//    {
//        _keyRotate = true;
//        _delta.y = -da;
//    }
}

void GameLayer::onKeyReleased(EventKeyboard::KeyCode keyCode, Event* unused_event)
{
//    _keyTranslate = false;
//    _keyRotate = false;
}

bool GameLayer::checkForLevelCompletion(cocos2d::PhysicsWorld& world, cocos2d::PhysicsShape& shape, void* data)
{
    
}

bool GameLayer::levelCompleted(cocos2d::PhysicsWorld& world, cocos2d::PhysicsShape& shape, void* data)
{
    
    _threadSafeCallbacks.push_back( CC_CALLBACK_0(GameLayer::doLevelCompleted, this) );
    
    return true;
}

bool GameLayer::doLevelCompleted()
{
    auto completeLevel = Label::createWithTTF("Stack Complete", "fonts/Marker Felt.ttf", 48);
//    completeLevel->setPosition(VisibleRect::center());
    this->addChild(completeLevel);
    
    _scene->simulationPause();
//    this->pause();
    
    return true;
    
}

void GameLayer::toggleDebugCallback(bool debugDraw)
{
    
    BeamBodyDrawNodesTiterator last = _beambodyDrawNodesT.end();
    for (BeamBodyDrawNodesTiterator iter = _beambodyDrawNodesT.begin(); iter != last; ++iter )
    {
        iter->second->setWireFrame(debugDraw);
    }
    
    SoftBodyDrawNodesIterator sblast = _softbodyDrawNodes.end();
    for (SoftBodyDrawNodesIterator iter = _softbodyDrawNodes.begin(); iter != sblast; ++iter )
    {
        iter->second->setWireFrame(debugDraw);
    }

    
}

