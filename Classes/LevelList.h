//
//  LevelList.hpp
//  Slingshot
//
//  Created by Federico Spadoni on 24/10/16.
//
//

#ifndef LevelList_hpp
#define LevelList_hpp


#include "extensions/GUI/CCScrollView/CCTableView.h"
#include "base/CCTouch.h"

//#include "LevelScene.h"

class LevelScene;
class LevelLoader;

namespace cocos2d {
    class Scene;
}

class LevelCustomTableView : public cocos2d::extension::TableView
{
public:
    static LevelCustomTableView* create(cocos2d::extension::TableViewDataSource* dataSource, cocos2d::Size size);
    
    virtual void onTouchEnded(cocos2d::Touch *touch, cocos2d::Event *event) override;
    
    void onMouseScroll(cocos2d::Event *event);
    
    enum {
        _TABEL_LABEL_TAG = 1024
    };
    
    
protected:
    LevelCustomTableView();
    

};



class LevelList : public cocos2d::extension::TableViewDataSource, public cocos2d::extension::TableViewDelegate
{
public:
    LevelList();
    
    void addLevel(const std::string& testName, std::function<cocos2d::Scene*(LevelLoader*)> callback);
    
    virtual void display();
    
    
    virtual void tableCellTouched(cocos2d::extension::TableView* table, cocos2d::extension::TableViewCell* cell) override;
    virtual cocos2d::extension::TableViewCell* tableCellAtIndex(cocos2d::extension::TableView *table, ssize_t idx) override;
    virtual cocos2d::Size tableCellSizeForIndex(cocos2d::extension::TableView *table, ssize_t idx) override;
    virtual ssize_t numberOfCellsInTableView(cocos2d::extension::TableView *table) override;
    
    virtual void scrollViewDidScroll(cocos2d::extension::ScrollView* view) override{}
    virtual void scrollViewDidZoom(cocos2d::extension::ScrollView* view) override{}
    
    void onBacksToMainMenu(cocos2d::Ref* sender);
    
private:
    
    std::vector<std::string> _levelNames;
    std::vector<std::function<cocos2d::Scene*(LevelLoader* loader)>> _levelCallbacks;
    bool _cellTouchEnabled;
    bool _shouldRestoreTableOffset;
    cocos2d::Vec2 _tableOffset;

};




#endif /* LevelList_hpp */
