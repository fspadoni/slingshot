//
//  GameConfig.h
//  game
//
//  Created by Federico Spadoni on 24/02/15.
//
//
#ifndef __game__GameConfig__
#define __game__GameConfig__

#include "cocos2d.h"


namespace CollisionCategory
{
    static unsigned int none = 0;
    static unsigned int actor = 1;
    static unsigned int slingshotSegments = 2;
    static unsigned int slingshotNodes = 4;
    static unsigned int platform = 8;
    
    static unsigned int all = ~(unsigned int)0;
}


class GameConfig : public cocos2d::Ref
{
    
public:
 
    static GameConfig* getInstance();
    
    static void destroyInstance();
 
    
    
    enum {
        STARTUP_MEMORY_SIZE_ALLOCATED = 65536 * 128
    };
    
    cocos2d::Size _designSize;

    cocos2d::Vec2 _gravity;
    
    
    std::string _slingShotPolyLine;
    std::string _slingShotMaterial;
    
    cocos2d::Vec2 _slingShotPosition;
    
    
    std::string _asteroidMesh;
    std::string _asteroidMaterial;
    cocos2d::Vec2 _asteroidPosition;
    float _asteroidOrientation;
    cocos2d::Vec2 _asteroidScale;
    
    std::string _planetMesh;
    std::string _planetMaterial[3];
    cocos2d::Vec2 _planetPosition;
    cocos2d::Vec2 _planetScale;
    
    float _bulletSize;
    float _bulletMass;
    float _bulletMoment;
    float _bulletElasticity;
    float _bulletFriction;
    
    float _goalSize;
 
    
    float _sideBoardsWidth;
    float _sideBoardsThickness;

    float _hashCellSize;
    float _hashCellCount;
    
    
CC_CONSTRUCTOR_ACCESS:
    GameConfig();
    
    virtual ~GameConfig();
    
private:
    
    static GameConfig* s_singleInstance;
    
};


#endif /* defined(__game__GameConfig__) */
