//
//  SlingShot.cpp
//  Slingshot
//
//  Created by Federico Spadoni on 24/01/16.
//
//

#include "SlingShot.h"

#include "GameConfig.h"
//#include "GameLayer.h"
#include "GameScene.h"
#include "physics/CCMaterialProperty.h"
#include "physics/CCPhysicsRigidBody.h"

#include "chipmunk.h"

USING_NS_CC;

ActiveStatus SlingShot::Active;
DrawingStatus SlingShot::Drawing;
//ShootingStatus SlingShot::Shooting;


SlingShot* SlingShot::createFromPolyLine(cocos2d::PolyLine* polyLine, cocos2d::BeamBodyMaterial* material)
{
    SlingShot* slingShot = new (std::nothrow) SlingShot();
    
    if (slingShot && slingShot->init(polyLine, material) )
    {
        slingShot->autorelease();
    }
    else{
        CC_SAFE_RELEASE(slingShot);
        return nullptr;
    }
    
    return slingShot;
}


SlingShot* SlingShot::clone(const SlingShot* other)
{
    SlingShot* block = new (std::nothrow) SlingShot();
    
    if (block && block->copy(other) )
    {
        block->autorelease();
    }
    else{
        CC_SAFE_RELEASE(block);
        return nullptr;
    }
    
    return block;
}


bool SlingShot::init(cocos2d::PolyLine* polyLine, cocos2d::BeamBodyMaterial* material)
{
    bool ret = PhysicsBeamBody::init(polyLine, material);
    
    endPointIndices[0] =  0;
    endPointIndices[1] =  getNumNodes() - 1;
    
    notifyBreakElement(true);
    notifyBreak(true);
    notifyBeginCollision(true);
    notifySeparateCollision(false);
    
    return ret;
}


SlingShot::SlingShot()
: PhysicsBeamBody()
, _currentStatus(&Active)
, _drawingNodeId(0)
{
    //    setSleepCallback( std::bind(&Block::breakAllElement, this, std::placeholders::_1) );
}

SlingShot::~SlingShot()
{
    
}


bool SlingShot::beginDrawing(cocos2d::PhysicsWorld* physicsWorld, const Vec2& point, const float& maxDistance)
{
    cpPointQueryInfo info;
//    query only the slingshot collision
    physicsWorld->PhysicsWorld::queryPointNearest( point, maxDistance, CollisionCategory::slingshotNodes, CollisionCategory::slingshotNodes, &info );
    
    
    if ( const cpShape* shape = info.shape ) // && shape->userData != NULL )
    {
//        skip many test because the collision filter only the slingshot
        
        cpBody *body = cpShapeGetBody(shape);
        
//        cpBeamBody* beambody = (cpBeamBody*) cpBodyGetSoftBody(body);
        cpBeamPointShape* pointShape = (cpBeamPointShape*)cpBodyGetUserData(body);
        
        _drawingNodeId = cpBeamPointShapeGetIndex(pointShape);
        
        PhysicsBeamBody::addConstraintVelocity( _drawingNodeId, Vec2(0.0, 0.0) );

    }

    
    
    return false;
}


bool SlingShot::draw(cocos2d::PhysicsWorld* physicsWorld, Vec2& velocity )
{
    
    PhysicsBeamBody::setNodeVelocity( velocity, _drawingNodeId );
    PhysicsBeamBody::setNodeVelocity( Vec2(0.0, 0.0), endPointIndices[0] );
    PhysicsBeamBody::setNodeVelocity( Vec2(0.0, 0.0), endPointIndices[1] );
    
    return true;
}

bool SlingShot::move(cocos2d::PhysicsWorld* physicsWorld, Vec2& velocity )
{
    
    static const float minX = Director::getInstance()->getVisibleOrigin().x + GameConfig::getInstance()->_sideBoardsThickness/2.0 + 2.0;
    
    static const float maxX = Director::getInstance()->getVisibleOrigin().x + Director::getInstance()->getVisibleSize().width - GameConfig::getInstance()->_sideBoardsThickness/2.0 - 2.0;

    static const cpVect3* nodePositions = PhysicsBeamBody::getNodes();
    
    if ( nodePositions[endPointIndices[0]].x <= minX )
    {
        velocity.x = std::max<float>( velocity.x, 0.0 );
    }
    
    if ( nodePositions[endPointIndices[1]].x >= maxX )
    {
        velocity.x = std::min<float>( velocity.x, 0.0 );
    }

    
    PhysicsBeamBody::setNodeVelocity( velocity, _drawingNodeId );
    PhysicsBeamBody::setNodeVelocity( Vec2(velocity.x, 0), endPointIndices[0] );
    PhysicsBeamBody::setNodeVelocity( Vec2(velocity.x, 0), endPointIndices[1] );
    
    return true;
}


bool SlingShot::stopMoving(cocos2d::PhysicsWorld* physicsWorld )
{
    PhysicsBeamBody::removeConstraintVelocity( _drawingNodeId );    
    PhysicsBeamBody::setNodeVelocity( Vec2(0.0, 0.0), endPointIndices[0] );
    PhysicsBeamBody::setNodeVelocity( Vec2(0.0, 0.0), endPointIndices[1] );
    
    return true;
}

bool SlingShot::shoot(cocos2d::PhysicsWorld* physicsWorld)
{

    PhysicsBeamBody::removeConstraintVelocity(_drawingNodeId);
    
    changeStatus(&Active);
    
    updateStatus();
    
    return true;
}


void SlingShot::sleepBegin(void)
{
    
}

void SlingShot::breakElement(const unsigned short triangleIdx)
{
//    _gameLayer->increaseBlockFragmentCounter();
}

void SlingShot::breakAllElement()
{


}



void SlingShot::collisionBegin( cpBody* collidingBody, cpArbiter* arb )
{
    
    std::set<const cocos2d::PhysicsRigidBody*>::const_iterator lastBullet = _bullets.end();
    for ( std::set<const cocos2d::PhysicsRigidBody*>::const_iterator bulletIter=_bullets.begin(); bulletIter!=lastBullet; ++bulletIter )
    {
        if ( collidingBody == (*bulletIter)->getCPBody() )
        {
            changeStatus(&Drawing);
            updateStatus();
            break;
        }
    }
    
    if (const cpSoftBodyOpt* collidingSoftBody = cpBodyGetSoftBody(collidingBody) )
    {
        
    }
    
}

void SlingShot::collisionSeparate( cpBody* collidingBody, cpArbiter* arb )
{
    std::set<const cocos2d::PhysicsRigidBody*>::const_iterator lastBullet = _bullets.end();
    for ( std::set<const cocos2d::PhysicsRigidBody*>::const_iterator bulletIter=_bullets.begin(); bulletIter!=lastBullet; ++bulletIter )
    {
        if ( collidingBody == (*bulletIter)->getCPBody() )
        {
            break;
        }
    }
    
}



void SlingShot::changeStatus(SlingShotStatus* newStatus)
{
    _currentStatus->exit(this);
    
    _currentStatus = newStatus;
    
    _currentStatus->enter(this);
}

void SlingShot::updateStatus()
{
    if ( _currentStatus )
    {
        _currentStatus->execute(this);
    }
}