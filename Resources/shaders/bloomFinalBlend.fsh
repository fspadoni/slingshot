
#ifdef GL_ES
precision lowp float;
#endif

uniform float radius;
varying vec2 v_texCoord;
varying vec4 v_fragmentColor;


vec4 adjustSaturation(vec4 color, float saturation)
{
    // The constants 0.3, 0.59, and 0.11 are chosen because the
    // human eye is more sensitive to green light, and less to blue.
    const vec3 sensitivity = vec3(0.3, 0.59, 0.11);
    float grey = dot(color.xyz, sensitivity );
    
    return vec4( mix(grey, color.x, saturation), mix(grey, color.y, saturation), mix(grey, color.z, saturation), color.w );
}


void main()
{
    const float bloomSaturation = 1.5;
    const float baseSaturation = 1.0;
    const float bloomIntensity = 2.0;
    const float baseIntensity = 1.0;
    
    vec4 baseColor = texture2D(CC_Texture0, v_texCoord);
    vec4 bloomColor = texture2D(CC_Texture1, v_texCoord);
    
    // Adjust color saturation and intensity.
    baseColor = adjustSaturation(baseColor, baseSaturation) * baseIntensity;
    bloomColor = adjustSaturation(bloomColor, bloomSaturation) * bloomIntensity;
    
    // Darken down the base image in areas where there is a lot of bloom,
    // to prevent things looking excessively burned-out.
    baseColor *= (1.0 - clamp(bloomColor, 0.0, 1.0));
    
//    gl_FragColor = texture2D(CC_Texture1, v_texCoord);// + baseIntensity*texture2D(CC_Texture0, v_texCoord);
    gl_FragColor = baseColor + bloomColor;
}
