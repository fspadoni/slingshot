#ifdef GL_ES
precision lowp float;
#endif

//uniform float bloomThreshold;
varying vec2 v_texCoord;
varying vec4 v_fragmentColor;
varying vec2 v_blurTexCoords[14];

uniform float weight[8]; //  = { 0.118, 0.113, 0.100, 0.082, 0.061, 0.042, 0.027, 0.016 };

const float intensity = 1.0; //0.635;

void main()
{
    
    vec4 sum = vec4(0);
    
    
    //thank you! http://xissburg.com/faster-gaussian-blur-in-glsl/
    
 
    sum += texture2D(CC_Texture0, v_blurTexCoords[ 0]) * weight[7];
    sum += texture2D(CC_Texture0, v_blurTexCoords[ 1]) * weight[6];
    sum += texture2D(CC_Texture0, v_blurTexCoords[ 2]) * weight[5];
    sum += texture2D(CC_Texture0, v_blurTexCoords[ 3]) * weight[4];
    sum += texture2D(CC_Texture0, v_blurTexCoords[ 4]) * weight[3];
    sum += texture2D(CC_Texture0, v_blurTexCoords[ 5]) * weight[2];
    sum += texture2D(CC_Texture0, v_blurTexCoords[ 6]) * weight[1];
    sum += texture2D(CC_Texture0, v_texCoord         ) * weight[0];
    sum += texture2D(CC_Texture0, v_blurTexCoords[ 7]) * weight[1];
    sum += texture2D(CC_Texture0, v_blurTexCoords[ 8]) * weight[2];
    sum += texture2D(CC_Texture0, v_blurTexCoords[ 9]) * weight[3];
    sum += texture2D(CC_Texture0, v_blurTexCoords[10]) * weight[4];
    sum += texture2D(CC_Texture0, v_blurTexCoords[11]) * weight[5];
    sum += texture2D(CC_Texture0, v_blurTexCoords[12]) * weight[6];
    sum += texture2D(CC_Texture0, v_blurTexCoords[13]) * weight[7];
    
    //increase blur with intensity!
    gl_FragColor = sum*intensity;// + texture2D(CC_Texture0, texcoord);
//    //    gl_FragColor = color;
}
