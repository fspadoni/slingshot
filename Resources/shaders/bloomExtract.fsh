#ifdef GL_ES
precision lowp float;
#endif

uniform float bloomThreshold;
varying vec2 v_texCoord;
varying vec4 v_fragmentColor;


const float threshold = 0.750;

void main()
{
    vec4 color = texture2D(CC_Texture0, v_texCoord);
    gl_FragColor = clamp( (color - threshold) / (1.0 - threshold), 0.0, 1.0);
    //    gl_FragColor = saturate((c - bloomThreshold) / (1.0 - bloomThreshold));
    //    gl_FragColor = texture2D(CC_Texture0, v_texCoord);
    //    gl_FragColor = texture2D(CC_Texture1, v_texCoord) + texture2D(CC_Texture0, v_texCoord);
    //    gl_FragColor = vec4(0.5,0.5,0.5,0.5);
}
